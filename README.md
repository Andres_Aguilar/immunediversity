** Immunediversity **
=====================


 Immunediversity port from R to Java
---------------------------------------


** Dependencies **
------------------

* IgBLAST 2.2.26+

* HMMER 3

* Usearch v7

** Configuration file **
-------------------------

> /\* config 4 IgBLAST \*/
>
> blastBinPath=/opt/ImmunediveRsity/data/IGBLAST/
>
> /\* Human DataBases \*/  
>
> HumanHeavyChainVDataBase=/Scripts/DataBases/human/HC/IgHV
>
> HumanHeavyChainDDataBase=/Scripts/DataBases/human/HC/IgHD
>
> HumanHeavyChainJDataBase=/Scripts/DataBases/human/HC/IgHJ
>
>
> HumanKappaVDataBase=/Scripts/DataBases/human/LC/kappa/IgKV
>
> HumanKappaJDataBase=/Scripts/DataBases/human/LC/kappa/IgKJ
>
>
> HumanLamdaVDataBase=/Scripts/DataBases/human/LC/lamda/IgLV
>
> HumanLamdaJDataBase=/Scripts/DataBases/human/LC/lamda/IgLV
>
>
> blastHumanAuxData=/Scripts/DataBases/human/human_gl.aux
>
> /\* Mouse DataBases \*/
>
> MouseHeavyChainVDataBase=/Scripts/DataBases/mouse/HC/IgHV
>
> MouseHeavyChainDDataBase=/Scripts/DataBases/mouse/HC/IgHD
>
> MouseHeavyChainJDataBase=/Scripts/DataBases/mouse/HC/IgHV
>
> MouseKappaVDataBase=
>
> MouseKappaJDataBase=
>
> MouseLamdaVDataBase=
>
> MouseLamdaJDataBase=
>
> blastMouseAuxData=/Scripts/DataBases/mouse/mouse_gl.aux
>
> /\* config 4 HMMER \*/
>
> HmmerBinPath=/usr/local/bin/
>
> /\* HMMER Human profils \*/
>
> HumanHmmerHCVProfile=/HC_HMMER_Profil/HC_HMMER_Profil/IgHV.hmm
>
> HumanHmmerHCJProfile=/HC_HMMER_Profil/HC_HMMER_Profil/IgHJ.hmm
>
> HumanHmmerKVProfil=/LC_HMMER_Profil/Kappa/KappaHMMERProfil/IGKV_V99.hmm
>
> HumanHmmerKJProfil=/LC_HMMER_Profil/Kappa/KappaHMMERProfil/IGKJ_V99.hmm
>
> HumanHmmerLVProfil=/LC_HMMER_Profil/Lamda/LamdaHMMERProfil/IgLV.hmm
>
> HumanHmmerLJProfil=/LC_HMMER_Profil/Lamda/LamdaHMMERProfil/IgLJ.hmm
>
> /\* HMMER mouse profils \*/
>
> MouseHmmerHCVProfile=
>
> MouseHmmerHCJProfile=
>
> MouseHmmerKVProfil=
>
> MouseHmmerKJProfil=
>
> MouseHmmerLVProfil=
>
> MouseHmmerLJProfil=
>
> /\* USEARCH \*/
>
> UsearchBin=/opt/ImmunediveRsity/bin/
>
