/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.IO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.biojava3.core.sequence.DNASequence;
import org.biojava3.core.sequence.ProteinSequence;
import org.biojava3.core.sequence.compound.NucleotideCompound;
import org.biojava3.core.sequence.features.FeatureInterface;
import org.biojava3.core.sequence.features.QualityFeature;
import org.biojava3.core.sequence.io.FastaWriterHelper;
import org.biojava3.core.sequence.template.AbstractSequence;
import org.biojava3.sequencing.io.fastq.Fastq;
import org.biojava3.sequencing.io.fastq.FastqBuilder;
import org.biojava3.sequencing.io.fastq.FastqReader;
import org.biojava3.sequencing.io.fastq.FastqTools;
import org.biojava3.sequencing.io.fastq.FastqVariant;
import org.biojava3.sequencing.io.fastq.FastqWriter;
import org.biojava3.sequencing.io.fastq.IlluminaFastqReader;
import org.biojava3.sequencing.io.fastq.IlluminaFastqWriter;
import org.biojava3.sequencing.io.fastq.SangerFastqReader;
import org.biojava3.sequencing.io.fastq.SangerFastqWriter;
import org.biojava3.sequencing.io.fastq.SolexaFastqReader;
import org.biojava3.sequencing.io.fastq.SolexaFastqWriter;
import org.immunediversity.beans.ClonalGroupTemp;
import org.immunediversity.beans.HMMERout;
import org.immunediversity.beans.HeavyChain;
import org.immunediversity.beans.Lineage;
import org.immunediversity.beans.Rearrangement;
import org.immunediversity.util.FastqQualityTools;
import org.immunediversity.util.ObjectSequenceConverter;
import it.unimi.dsi.fastutil.objects.Reference2ObjectOpenHashMap;
import java.util.Map;
import org.immunediversity.beans.HeavyChain2;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class Writers {
    /* Standar methods */

    public void writeNucleotideFasta(List<DNASequence> sequences, File outFile) throws Exception {
        FastaWriterHelper.writeNucleotideSequence(outFile, sequences);
    }

    public void writeNucleotideFasta(LinkedHashMap<String, DNASequence> sequences, File outFile) throws Exception {
        ObjectSequenceConverter converter = new ObjectSequenceConverter();
        List<DNASequence> sequence = converter.LinkedHashMap2List(sequences);
        FastaWriterHelper.writeNucleotideSequence(outFile, sequence);
    }

    public void writeProteinFasta(List<ProteinSequence> sequences, File outFile) throws Exception {
        FastaWriterHelper.writeProteinSequence(outFile, sequences);
    }

    public boolean writeFastq(List<DNASequence> sequences, File outFile, String variant) throws IOException {
        boolean status = false;
        Iterator<DNASequence> iterator = sequences.iterator();
        LinkedHashMap qualityMap;

        FastqWriter fastqWriter;

        if (variant.equalsIgnoreCase("illumina")) {
            fastqWriter = new IlluminaFastqWriter();
            qualityMap = FastqQualityTools.illumina18();

        } else if (variant.equalsIgnoreCase("solexa")) {
            fastqWriter = new SolexaFastqWriter();
            qualityMap = FastqQualityTools.solexa();

        } else {
            fastqWriter = new SangerFastqWriter();
            qualityMap = FastqQualityTools.sanger();
        }

        try (FileWriter fileWriter = new FileWriter(outFile)) {
            while (iterator.hasNext()) {
                DNASequence sequence = iterator.next();

                String seq = sequence.getSequenceAsString();
                String qual = "";

                // get sequence quality feature
                List<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> features = sequence.getFeatures();
                Iterator<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> it2 = features.iterator();

                if (it2.hasNext()) {
                    QualityFeature quality = (QualityFeature) it2.next();
                    List qualities = quality.getQualities();

                    // quality mean
                    Iterator qIt = qualities.iterator();
                    while (qIt.hasNext()) {
                        qual += qualityMap.get(qIt.next().toString());
                    }
                }

                FastqBuilder f = new FastqBuilder();
                f.appendSequence(seq);
                f.appendQuality(qual);

                f = f.withDescription(sequence.getOriginalHeader());

                if (variant.equalsIgnoreCase("illumina")) {
                    f = f.withVariant(FastqVariant.FASTQ_ILLUMINA);
                } else if (variant.equalsIgnoreCase("solexa")) {
                    f = f.withVariant(FastqVariant.FASTQ_SOLEXA);
                } else {
                    f = f.withVariant(FastqVariant.FASTQ_SANGER);
                }

                Fastq fastq = f.build();
                fastqWriter.append(fileWriter, fastq);
            }

            fileWriter.flush();
            status = true;
        }

        return status;
    }

    /* Immunediversity methods */
    /**
     * @param igs LinkedHashMap of HeavyChains
     * @param outPut output file
     * @throws java.io.FileNotFoundException
     */
    public void writeVDJTSVFile(LinkedHashMap<String, HeavyChain> igs, File outPut) throws FileNotFoundException, IOException {
        if (!igs.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outPut), Charset.defaultCharset()))) {
                Set set = igs.keySet();
                Iterator<String> it = set.iterator();

                // write header
                writer.write("read\tV\tD\tJ\n");
                while (it.hasNext()) {
                    HeavyChain heavyChain = igs.get(it.next());

                    String line = "";

                    line += heavyChain.getID() + "\t";

                    if (heavyChain.hasVgene()) {
                        line += heavyChain.getVgene() + "*" + heavyChain.getAlleleV() + "\t";
                    } else {
                        line += "\t";
                    }

                    if (heavyChain.hasDgene()) {
                        line += heavyChain.getDgene() + "*" + heavyChain.getAlleleD() + "\t";
                    } else {
                        line += "\t";
                    }

                    if (heavyChain.hasJgene()) {
                        line += heavyChain.getJgene() + "*" + heavyChain.getAlleleJ();
                    } else {
                        line += "\t";
                    }

                    line += "\n";

                    writer.write(line);
                }
                writer.flush();
            }
        }
    }

    /**
     * @param heavyChains LinkedHashMap with HeavyChain objects
     * @param output output fasta file
     * @throws java.io.FileNotFoundException
     */
    public void writeCDR3file(LinkedHashMap<String, HeavyChain> heavyChains, File output) throws FileNotFoundException, IOException {
        if (!heavyChains.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), Charset.defaultCharset()))) {
                Set set = heavyChains.keySet();
                Iterator<String> it = set.iterator();

                while (it.hasNext()) {
                    HeavyChain heavyChain = heavyChains.get(it.next());

                    if (heavyChain.hasCDR3Sequence()) {
                        writer.write(">" + heavyChain.getID() + "\n");
                        writer.write(heavyChain.getCdr3().getSequenceAsString() + "\n");
                    }
                }
                writer.flush();
            }
        }
    }

    /**
     * @param rearrangement
     * @param output
     * @throws java.lang.Exception
     */
    public void writeRearrangementFile(Rearrangement rearrangement, File output)
            throws Exception {

        LinkedHashMap<String, DNASequence> cdr3s = new LinkedHashMap();

        if (rearrangement != null) {
            LinkedList<HeavyChain> heavyChains = rearrangement.getHeavyChains();

            if (!heavyChains.isEmpty()) {
                Iterator<HeavyChain> it = heavyChains.iterator();

                while (it.hasNext()) {
                    HeavyChain hc = it.next();

                    if (hc.hasCDR3Sequence()) {
                        DNASequence cdr3 = hc.getCdr3();
                        cdr3.setOriginalHeader(hc.getID());

                        cdr3s.put(hc.getID(), cdr3);
                    }
                }

                if (!cdr3s.isEmpty()) {
                    writeNucleotideFasta(cdr3s, output);
                }
            }
        }
    }

    /**
     * @param clonalGroups
     * @param libName
     * @param outPut
     * @throws java.io.FileNotFoundException
     */
    public void writeClonalGropsFile(LinkedHashMap<String, ClonalGroupTemp> clonalGroups, String libName, File outPut) throws FileNotFoundException, IOException {
        if (!clonalGroups.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outPut), Charset.defaultCharset()))) {
                Set cgSet = clonalGroups.keySet();
                Iterator<String> cgIt = cgSet.iterator();

                while (cgIt.hasNext()) {
                    String cgID = cgIt.next();
                    ClonalGroupTemp clonalGroup = clonalGroups.get(cgID);

                    String clonalGroupName = clonalGroup.getName();

                    LinkedHashMap<String, HeavyChain> heavyChains = clonalGroup.getHeavyChains();
                    Set hcSet = heavyChains.keySet();
                    Iterator<String> hcIt = hcSet.iterator();

                    while (hcIt.hasNext()) {
                        String hcID = hcIt.next();
                        HeavyChain hc = heavyChains.get(hcID);

                        writer.write(hc.getID() + "." + libName + "." + clonalGroupName + "\n");
                    }
                }
                writer.flush();
            }
        }
    }

    /**
     * @param clonalGroups
     * @param libName
     * @param outPut
     * @throws java.io.FileNotFoundException
     */
    public void writeClonalGroupsConsensusFile(LinkedHashMap<String, ClonalGroupTemp> clonalGroups, String libName, File outPut) throws FileNotFoundException, IOException {
        if (!clonalGroups.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outPut), Charset.defaultCharset()))) {
                Set cgSet = clonalGroups.keySet();
                Iterator<String> cgIt = cgSet.iterator();

                while (cgIt.hasNext()) {
                    String cgID = cgIt.next();
                    ClonalGroupTemp clonalGroup = clonalGroups.get(cgID);

                    String clonalGroupName = clonalGroup.getName();

                    writer.write(">" + libName + "." + clonalGroupName + "\n");
                    writer.write(clonalGroup.getConsensusSequence().getSequenceAsString() + "\n");
                }
                writer.flush();
            }
        }
    }

    public void writeLineagesFile(LinkedHashMap<String, HeavyChain> heavyChains, File output) throws FileNotFoundException, IOException {
        if (!heavyChains.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), Charset.defaultCharset()))) {
                Set set = heavyChains.keySet();
                Iterator<String> it = set.iterator();

                while (it.hasNext()) {
                    HeavyChain heavyChain = heavyChains.get(it.next());

                    if (heavyChain.hasCDR3Sequence()) {
                        writer.write(">" + heavyChain.getID() + "\n");
                        writer.write(heavyChain.getSequence().getSequenceAsString() + "\n");
                    }
                }
                writer.flush();
            }
        }
    }

    public void writeGroupsFullVJ(LinkedHashMap<String, Lineage> lineages, String libName, File output) throws FileNotFoundException, IOException {
        if (!lineages.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), Charset.defaultCharset()))) {
                Set set = lineages.keySet();
                Iterator<String> it = set.iterator();

                while (it.hasNext()) {
                    String id = it.next();
                    Lineage lineage = lineages.get(id);

                    LinkedHashMap<String, HeavyChain> heavyChains = lineage.getHeavyChains();
                    Set hcSet = heavyChains.keySet();
                    Iterator<String> hcIt = hcSet.iterator();

                    while (hcIt.hasNext()) {
                        HeavyChain hc = heavyChains.get(hcIt.next());

                        writer.write(hc.getID() + "." + libName + "." + lineage.getName() + "\n");
                    }
                }
                writer.flush();
            }
        }
    }

    public void writeConsensusFullVJ(LinkedHashMap<String, Lineage> lineages, String libName, File output) throws FileNotFoundException, IOException {
        if (!lineages.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), Charset.defaultCharset()))) {
                Set set = lineages.keySet();
                Iterator<String> it = set.iterator();

                while (it.hasNext()) {
                    String id = it.next();
                    Lineage lineage = lineages.get(id);

                    writer.write(">" + libName + "." + lineage.getName() + "\n");
                    writer.write(lineage.getConsensusSequence().getSequenceAsString() + "\n");
                }
                writer.flush();
            }
        }
    }

    /**
     * @param igs
     * @param output
     * @throws java.io.FileNotFoundException
     */
    public void writeOnlyV(LinkedHashMap<String, HeavyChain> igs, File output) throws FileNotFoundException, IOException {
        if (!igs.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), Charset.defaultCharset()))) {
                Set set = igs.keySet();
                Iterator<String> it = set.iterator();

                while (it.hasNext()) {
                    HeavyChain hc = igs.get(it.next());

                    writer.write(">" + hc.getID() + "\n");
                    String sequence = hc.getSequence().getSequenceAsString();
                    String onlyV = sequence.substring(hc.getStartV() - 1, hc.getEndV());

                    writer.write(onlyV + "\n");
                }
                writer.flush();
            }
        }
    }

    /**
     * Write a sequence from 2 Strings(id and sequence)
     *
     * @param id Sequence ID
     * @param sequence Sequence as string
     * @param output output file
     * @throws java.io.FileNotFoundException
     */
    public void writeSequence(String id, String sequence, File output) throws FileNotFoundException, IOException {
        if (!id.isEmpty() && !sequence.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), Charset.defaultCharset()))) {
                writer.write(">" + id + "\n");
                writer.write(sequence + "\n");

                writer.flush();
            }
        }
    }

    public void writeMutationsFile(LinkedList<String> mutations, File outputFile) throws FileNotFoundException, IOException {
        if (!mutations.isEmpty()) {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), Charset.defaultCharset()))) {
                Iterator<String> it = mutations.iterator();

                writer.write("ID\tks\tka\n");
                while (it.hasNext()) {
                    String string = it.next();
                    writer.write(string + "\n");
                }
                writer.flush();
            }
        }
    }

    /* New version */
    /**
     * @param fastq
     * @param fasta
     * @param variant
     * @return
     * @throws java.io.FileNotFoundException
     */
    public int writeFastq2Fasta(File fastq, File fasta, String variant) throws FileNotFoundException, IOException {
        int count = 0;
        if (fastq.exists()) {
            FastqReader fastqReader;

            // check fastq format
            if (variant.equalsIgnoreCase("illumina")) {
                fastqReader = new IlluminaFastqReader();
            } else if (variant.equalsIgnoreCase("solexa")) {
                fastqReader = new SolexaFastqReader();
            } else {
                fastqReader = new SangerFastqReader(); // read fastq(sanger)
            }

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fasta), Charset.defaultCharset()))) {

                for (Fastq fastqSeq : fastqReader.read(fastq)) {
                    DNASequence seq = FastqTools.createDNASequence(fastqSeq);
                    writer.write(">" + seq.getOriginalHeader() + "\n");
                    writer.write(seq.getSequenceAsString() + "\n");
                    count++;
                }

                writer.flush();
            }
        }
        return count;
    }

    /**
     * @param sequencesFile
     * @param outputFile
     * @param results
     * @param type
     * @throws java.io.IOException
     */
    public void writeCDR3FileStream(File sequencesFile, File outputFile, Reference2ObjectOpenHashMap<String, HMMERout> results, String type)
            throws IOException {
        if (sequencesFile.exists() && results != null) {
            // If file exist and can read
            if (sequencesFile.isFile() && sequencesFile.canRead()) {
                FastqReader fastqReader;
                // check fastq format
                if (type.equalsIgnoreCase("illumina")) {
                    fastqReader = new IlluminaFastqReader();
                } else if (type.equalsIgnoreCase("solexa")) {
                    fastqReader = new SolexaFastqReader();
                } else {
                    fastqReader = new SangerFastqReader(); // read fastq(sanger)
                }

                try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), Charset.defaultCharset()))) {
                    for (Fastq fastq : fastqReader.read(sequencesFile)) {
                        DNASequence seq = FastqTools.createDNASequenceWithQualityScores(fastq);

                        if (results.containsKey(seq.getOriginalHeader())) {
                            HMMERout hmm = results.get(seq.getOriginalHeader());
                            String cdr3Sequence = seq.getSequenceAsString();

                            int start = hmm.getStartAsInt();
                            int end = hmm.getEndAsInt() - 1;

                            if (start < end) {
                                writer.write(">" + seq.getOriginalHeader() + "\n");
                                writer.write(cdr3Sequence.substring(start, end) + "\n");
                            }
                        }
                    }
                    writer.flush();
                }
            }
        }
    }

    public void writeVDJsFile(Reference2ObjectOpenHashMap<String, HeavyChain2> heavyChainList, File outputFile, Reference2ObjectOpenHashMap<String, HMMERout> hmmerResults) throws IOException {
        if (!heavyChainList.isEmpty()) {

            try (Writer writer = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(outputFile), Charset.defaultCharset())
            )) {
                // write header
                writer.write("read\tV\tD\tJ\n");

                for (Map.Entry<String, HeavyChain2> entry : heavyChainList.entrySet()) {
                    HeavyChain2 hc = entry.getValue();
                    HMMERout hmm = hmmerResults.get(hc.getID());

                    if (hmm != null) {
                        if (hmm.getStartAsInt() < (hmm.getEndAsInt() - 1)) {
                            String line = "";

                            line += hc.getID() + "\t";

                            if (hc.hasVsegment()) {
                                line += hc.getvSegment() + "\t";
                            } else {
                                line += "\t";
                            }

                            if (hc.hasDsegment()) {
                                line += hc.getdSegment() + "\t";
                            } else {
                                line += "\t";
                            }

                            if (hc.hasJsegment()) {
                                line += hc.getjSegment();
                            } else {
                                line += "\t";
                            }

                            line += "\n";

                            writer.write(line);
                        }
                    }
                }
                writer.flush();
            }

        }
    }
}
