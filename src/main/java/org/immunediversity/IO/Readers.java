/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.IO;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import org.biojava3.core.sequence.DNASequence;
import org.biojava3.core.sequence.compound.DNACompoundSet;
import org.biojava3.core.sequence.compound.NucleotideCompound;
import org.biojava3.core.sequence.io.DNASequenceCreator;
import org.biojava3.core.sequence.io.FastaReader;
import org.biojava3.core.sequence.io.GenericFastaHeaderParser;
import org.biojava3.sequencing.io.fastq.Fastq;
import org.biojava3.sequencing.io.fastq.FastqReader;
import org.biojava3.sequencing.io.fastq.FastqTools;
import org.biojava3.sequencing.io.fastq.IlluminaFastqReader;
import org.biojava3.sequencing.io.fastq.SangerFastqReader;
import org.biojava3.sequencing.io.fastq.SolexaFastqReader;
import org.immunediversity.util.ObjectSequenceConverter;


/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class Readers {

    /**
     * @param inFile input Fastq file
     * @param type type or variant (sanger|solexa|illumina)
     * @return List with DNASequences
     * @throws java.io.IOException
     */
    public List<DNASequence> readListFastq(File inFile, String type)
            throws IOException {
        List<DNASequence> sequences = new LinkedList<>();

        // If file exist and can read
        if (inFile.isFile() && inFile.canRead()) {
            FastqReader fastqReader;

            // check fastq format
            if (type.equalsIgnoreCase("illumina")) {
                fastqReader = new IlluminaFastqReader();
            } else if (type.equalsIgnoreCase("solexa")) {
                fastqReader = new SolexaFastqReader();
            } else {
                fastqReader = new SangerFastqReader(); // read fastq(sanger)
            }

            for (Fastq fastq : fastqReader.read(inFile)) {
                sequences.add(FastqTools
                        .createDNASequenceWithQualityScores(fastq));
            }
        }
        return sequences;
    }

    public LinkedHashMap<String, DNASequence> readFastq2LinkedHashMap(File inFile, String type)
            throws IOException {
        LinkedHashMap<String, DNASequence> sequences = new LinkedHashMap();

        // If file exist and can read
        if (inFile.isFile() && inFile.canRead()) {
            FastqReader fastqReader;

            // check fastq format
            if (type.equalsIgnoreCase("illumina")) {
                fastqReader = new IlluminaFastqReader();
            } else if (type.equalsIgnoreCase("solexa")) {
                fastqReader = new SolexaFastqReader();
            } else {
                fastqReader = new SangerFastqReader();
            }

            for (Fastq fastq : fastqReader.read(inFile)) {
                sequences.put(fastq.getDescription(), FastqTools
                        .createDNASequenceWithQualityScores(fastq));
            }
        }

        return sequences;
    }

    /**
     * @param inFile input Fasta file
     * @return LinkedHashMap with all DNASequences
     * @throws java.io.IOException
     */
    public LinkedHashMap<String, DNASequence> readDNAFasta(File inFile)
            throws IOException {
        LinkedHashMap<String, DNASequence> sequences = new LinkedHashMap<>();

        // If file exist and can read
        if (inFile.isFile() && inFile.canRead()) {

            FileInputStream inStream = new FileInputStream(inFile);

            FastaReader<DNASequence, NucleotideCompound> fastaReader = new FastaReader<>(
                    inStream,
                    new GenericFastaHeaderParser<DNASequence, NucleotideCompound>(),
                    new DNASequenceCreator(DNACompoundSet.getDNACompoundSet()));

            sequences = fastaReader.process();
        }

        return sequences;
    }

    /**
     * @param inFile input Fasta file
     * @return List with all DNASequences
     * @throws java.io.IOException
     */
    public List<DNASequence> readListDNAFasta(File inFile)
            throws IOException {
        LinkedHashMap<String, DNASequence> sequences = new LinkedHashMap<>();

        // If file exist and can read
        if (inFile.isFile() && inFile.canRead()) {

            FileInputStream inStream = new FileInputStream(inFile);

            FastaReader<DNASequence, NucleotideCompound> fastaReader = new FastaReader<>(
                    inStream,
                    new GenericFastaHeaderParser<DNASequence, NucleotideCompound>(),
                    new DNASequenceCreator(DNACompoundSet.getDNACompoundSet()));

            sequences = fastaReader.process();
        }

        ObjectSequenceConverter converter = new ObjectSequenceConverter();
        return converter.LinkedHashMap2List(sequences);
    }

    
}
