/*
 * Copyright (C) 2015 Andres Aguilar <andresyoshimar@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.functions;

import com.google.common.io.Files;
import com.google.common.io.InputSupplier;
import it.unimi.dsi.fastutil.objects.Reference2ObjectOpenHashMap;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import org.biojava3.sequencing.io.fastq.Fastq;
import org.biojava3.sequencing.io.fastq.FastqBuilder;
import org.biojava3.sequencing.io.fastq.FastqReader;
import org.biojava3.sequencing.io.fastq.FastqWriter;
import org.biojava3.sequencing.io.fastq.IlluminaFastqReader;
import org.biojava3.sequencing.io.fastq.IlluminaFastqWriter;
import org.biojava3.sequencing.io.fastq.SangerFastqReader;
import org.biojava3.sequencing.io.fastq.SangerFastqWriter;
import org.biojava3.sequencing.io.fastq.SolexaFastqReader;
import org.biojava3.sequencing.io.fastq.SolexaFastqWriter;
import org.biojava3.sequencing.io.fastq.StreamListener;
import org.immunediversity.beans.BlastCoords;
import org.immunediversity.beans.IgKappa;
import org.immunediversity.beans.IgLamda;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class CutSequences {
    /* LIGHT CHAINS */
    
    @SuppressWarnings("ConvertToTryWithResources")
    public void cutFastqLCSequences(final LinkedHashMap<String, IgLamda> results, File inFile, File outFile, String variant) 
            throws IOException {
        if (inFile.exists() && inFile.canRead()) {
            if (results != null) {
                FastqReader fastqReader;
                InputSupplier inputSupplier = Files.newReaderSupplier(inFile, Charset.defaultCharset());

                final FastqWriter fastqWriter;
                final FileWriter fileWriter = new FileWriter(outFile);

                if (variant.equalsIgnoreCase("solexa")) {
                    fastqReader = new SolexaFastqReader();
                    fastqWriter = new SolexaFastqWriter();
                } else if (variant.equalsIgnoreCase("illumina")) {
                    fastqReader = new IlluminaFastqReader();
                    fastqWriter = new IlluminaFastqWriter();
                } else {
                    fastqReader = new SangerFastqReader();
                    fastqWriter = new SangerFastqWriter();
                }

                fastqReader.stream(inputSupplier, new StreamListener() {
                    @Override
                    public void fastq(Fastq fastq) {
                        String id = fastq.getDescription();
                        
                        IgLamda coords = results.get(id);
                        if (coords != null) {
                            if (coords.hasVgene() && coords.hasJgene()) {
                                // only cut if coords has V and J genes
                                String sequence = fastq.getSequence();
                                String quality = fastq.getQuality();

                                // coords to cut
                                int len = fastq.getSequence().length();
                                int start = coords.getStartV() -1;
                                int end = coords.getEndJ();

                                // Verifying coords
                                if (start < len && end <= len) {
                                    String seq = sequence.substring(start, end);
                                    String qual = quality.substring(start, end);

                                    if (seq.length() == qual.length()) {
                                        FastqBuilder fb = new FastqBuilder();
                                        fb = fb.withDescription(id);
                                        fb.appendSequence(seq);
                                        fb.appendQuality(qual);

                                        Fastq result = fb.build();
                                        try {
                                            fastqWriter.append(fileWriter, result);
                                        } catch (IOException ex) {
                                            System.err.println("ERROR: " + ex.getMessage());
                                        }
                                    }
                                }
                            }
                        } else {
                            System.err.println("WARNING: " + CutSequences.class.getName() + " coordinates not found for " + id + " sequence!!!");
                        }
                    }
                });

                fileWriter.flush();
                fileWriter.close();
            }
        }
    }
    
    /**
     * @param results LinkedHashMap with IgBlast results
     * @param inFile Fastq file to cut
     * @param outFile output Fastq file
     * @param variant Variant of Fastq (Fastq format). Default: Sanger
     * @throws java.io.IOException
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void cutFastqKCSequences(final LinkedHashMap<String, IgKappa> results, File inFile, File outFile, String variant)
            throws IOException {
        if (inFile.exists() && inFile.canRead()) {
            if (results != null) {
                FastqReader fastqReader;
                InputSupplier inputSupplier = Files.newReaderSupplier(inFile, Charset.defaultCharset());

                final FastqWriter fastqWriter;
                final FileWriter fileWriter = new FileWriter(outFile);

                if (variant.equalsIgnoreCase("solexa")) {
                    fastqReader = new SolexaFastqReader();
                    fastqWriter = new SolexaFastqWriter();
                } else if (variant.equalsIgnoreCase("illumina")) {
                    fastqReader = new IlluminaFastqReader();
                    fastqWriter = new IlluminaFastqWriter();
                } else {
                    fastqReader = new SangerFastqReader();
                    fastqWriter = new SangerFastqWriter();
                }

                fastqReader.stream(inputSupplier, new StreamListener() {
                    @Override
                    public void fastq(Fastq fastq) {
                        String id = fastq.getDescription();
                        
                        IgKappa coords = results.get(id);
                        if (coords != null) {
                            if (coords.hasVgene() && coords.hasJgene()) {
                                // only cut if coords has V and J genes
                                String sequence = fastq.getSequence();
                                String quality = fastq.getQuality();

                                // coords to cut
                                int len = fastq.getSequence().length();
                                int start = coords.getStartV() -1;
                                int end = coords.getEndJ();

                                // Verifying coords
                                if (start < len && end <= len) {
                                    String seq = sequence.substring(start, end);
                                    String qual = quality.substring(start, end);

                                    if (seq.length() == qual.length()) {
                                        FastqBuilder fb = new FastqBuilder();
                                        fb = fb.withDescription(id);
                                        fb.appendSequence(seq);
                                        fb.appendQuality(qual);

                                        Fastq result = fb.build();
                                        try {
                                            fastqWriter.append(fileWriter, result);
                                        } catch (IOException ex) {
                                            System.err.println("ERROR: " + ex.getMessage());
                                        }
                                    }
                                }
                            }
                        } else {
                            System.err.println("WARNING: " + CutSequences.class.getName() + " coordinates not found for " + id + " sequence!!!");
                        }
                    }
                });

                fileWriter.flush();
                fileWriter.close();
            }
        }
    }
    
    /* HEAVY CHAIN */
    /**
     * @param results LinkedHashMap with IgBlast results
     * @param inFile Fastq file to cut
     * @param outFile output Fastq file
     * @param variant Variant of Fastq (Fastq format). Default: Sanger
     * @throws java.io.IOException
     */
    @SuppressWarnings("ConvertToTryWithResources")
    public void cutFastqHCSequences(final Reference2ObjectOpenHashMap<String, BlastCoords> results, File inFile, File outFile, String variant)
            throws IOException {
        if (inFile.exists() && inFile.canRead()) {
            if (results != null) {
                FastqReader fastqReader;
                InputSupplier inputSupplier = Files.newReaderSupplier(inFile, Charset.defaultCharset());

                final FastqWriter fastqWriter;
                final FileWriter fileWriter = new FileWriter(outFile);

                if (variant.equalsIgnoreCase("solexa")) {
                    fastqReader = new SolexaFastqReader();
                    fastqWriter = new SolexaFastqWriter();
                } else if (variant.equalsIgnoreCase("illumina")) {
                    fastqReader = new IlluminaFastqReader();
                    fastqWriter = new IlluminaFastqWriter();
                } else {
                    fastqReader = new SangerFastqReader();
                    fastqWriter = new SangerFastqWriter();
                }

                fastqReader.stream(inputSupplier, new StreamListener() {
                    @Override
                    public void fastq(Fastq fastq) {
                        String id = fastq.getDescription();

                        BlastCoords coords = results.get(id.trim());
                        if (coords != null) {
                            if (coords.hasVgene() && coords.hasJgene()) {
                                // only cut if coords has V and J genes
                                String sequence = fastq.getSequence();
                                String quality = fastq.getQuality();

                                // coords to cut
                                int len = fastq.getSequence().length();
                                int start = coords.getvCoord() -1;
                                int end = coords.getjCoord();

                                // Verifying coords
                                if (start < len && end <= len) {
                                    String seq = sequence.substring(start, end);
                                    String qual = quality.substring(start, end);

                                    if (seq.length() == qual.length()) {
                                        FastqBuilder fb = new FastqBuilder();
                                        fb = fb.withDescription(id);
                                        fb.appendSequence(seq);
                                        fb.appendQuality(qual);

                                        Fastq result = fb.build();
                                        try {
                                            fastqWriter.append(fileWriter, result);
                                        } catch (IOException ex) {
                                            System.err.println("ERROR: " + ex.getMessage());
                                        }
                                    }
                                }
                            }
                        } else {
                            System.err.println("WARNING: " + CutSequences.class.getName() + " coordinates not found for " + id + " sequence!!!");
                        }
                    }
                });

                fileWriter.flush();
                fileWriter.close();
            }
        }
    }
}
