/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.beans;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import org.biojava3.core.sequence.DNASequence;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class Lineage {

    private String rearrangement = "";
    private LinkedHashMap<String, HeavyChain> heavyChains = new LinkedHashMap();
    private DNASequence consensusSequence;
    private int clonalGroupNumber = 0;
    private int lineageNumber = 0;
    
    private String alleleV = "";
    private String alleleD = "";
    private String alleleJ = "";
    
    private int nonSinonimousMutations = 0;
    private int sinonimousMutations = 0;
    
    /**
     * @param alleleV
     */
    public void setAlleleV(String alleleV) {
        this.alleleV = alleleV;
    }
    
    /**
     * @param alleleJ
     */
    public void setAlleleJ(String alleleJ) {
        this.alleleJ = alleleJ;
    }
    
    /**
     * @return allele V
     */
    public String getAlleleV() {
        return this.alleleV;
    }
    
    /**
     * @return allele J
     */
    public String getAlleleJ() {
        return this.alleleJ;
    }
    
    /**
     * @param mutations number of sinonimous mutations in V segment
     */
    public void setSinonimousMutations(int mutations) {
        this.sinonimousMutations = mutations;
    }
    
    /**
     * @param mutations non sinonimous mutations in V segment
     */
    public void setNonSinonimousMutations(int mutations) {
        this.nonSinonimousMutations = mutations;
    }

    /**
     * @return sinonimous mutations
     */
    public int getSinonimousMutations() {
        return this.sinonimousMutations;
    }
    
    /**
     * @return non sinonimous mutations
     */
    public int getNonSinonimousMutations() {
        return this.nonSinonimousMutations;
    }


    /**
     * @return the rearrangement
     */
    public String getRearrangement() {
        return rearrangement;
    }

    /**
     * @param rearrangement the rearrangement to set
     */
    public void setRearrangement(String rearrangement) {
        this.rearrangement = rearrangement;
    }

    /**
     * @return the heavyChains
     */
    public LinkedHashMap<String, HeavyChain> getHeavyChains() {
        return heavyChains;
    }

    /**
     * @param heavyChains the heavyChains to set
     */
    public void setHeavyChains(LinkedHashMap<String, HeavyChain> heavyChains) {
        this.heavyChains = heavyChains;
    }

    /**
     * @return the consensusSequence
     */
    public DNASequence getConsensusSequence() {
        return consensusSequence;
    }

    /**
     * @param consensusSequence the consensusSequence to set
     */
    public void setConsensusSequence(DNASequence consensusSequence) {
        this.consensusSequence = consensusSequence;
    }

    /**
     * @return the clonalGroupNumber
     */
    public int getClonalGroupNumber() {
        return clonalGroupNumber;
    }

    /**
     * @param clonalGroupNumber the clonalGroupNumber to set
     */
    public void setClonalGroupNumber(int clonalGroupNumber) {
        this.clonalGroupNumber = clonalGroupNumber;
    }

    /**
     * @return the lineageNumber
     */
    public int getLineageNumber() {
        return lineageNumber;
    }

    /**
     * @param lineageNumber the lineageNumber to set
     */
    public void setLineageNumber(int lineageNumber) {
        this.lineageNumber = lineageNumber;
    }

    /**
     * @return true if heavy chains are empty
     */
    public boolean isEmpty() {
        return heavyChains.isEmpty();
    }
    
    public boolean hasID(String id) {
        boolean res = false;
        
        id = id.replace("centroid=", "");
        id = id.replace(";", "");
        id = id.replaceAll("seqs=\\d+", "");
        
        if (!heavyChains.isEmpty()) {
            Set set = heavyChains.keySet();
            Iterator<String> it = set.iterator();
            
            while (it.hasNext()) {
                String hcID = it.next();
                res = id.equalsIgnoreCase(hcID);
                
                if (res) {
                    break;
                }
            }
        }
        
        return res;
    }

    public String getName(){
        String name = rearrangement + "." + clonalGroupNumber + "." + lineageNumber;
        
        return name;
    }
}
