/*
 * Copyright (C) 2015 Andres Aguilar <andresyoshimar@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.beans;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class BlastCoords {
    private int vCoord; 
    private int jCoord; 
    private String id;
    
    public BlastCoords() {}
    
    public BlastCoords(int vCoord, int jCoord) {
        this.vCoord = vCoord;
        this.jCoord = jCoord;
    }

    /**
     * @return the vCoord
     */
    public int getvCoord() {
        return vCoord;
    }

    /**
     * @param vCoord the vCoord to set
     */
    public void setvCoord(int vCoord) {
        this.vCoord = vCoord;
    }

    /**
     * @return the jCoord
     */
    public int getjCoord() {
        return jCoord;
    }

    /**
     * @param jCoord the jCoord to set
     */
    public void setjCoord(int jCoord) {
        this.jCoord = jCoord;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id.trim();
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id.trim();
    }
    
    public boolean hasVgene() {
        return this.vCoord != 0;
    }
    
    public boolean hasJgene() {
        return this.jCoord != 0;
    }
    
    @Override
    public String toString() {
        String result = "";
        result += "Blast coords for:\t" + this.getId();
        result += "\nStart V:\t" + this.getvCoord();
        result += "\nEnd J:\t" + this.getjCoord();
        
        return result;
    }
}
