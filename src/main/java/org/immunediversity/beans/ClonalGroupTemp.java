/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.beans;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import org.biojava3.core.sequence.DNASequence;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class ClonalGroupTemp {
    private String rearrangement = "";
    private LinkedHashMap<String, HeavyChain> heavyChains = new LinkedHashMap();
    private DNASequence consensusSequence;
    private int number;
    
    
    /**
     * @return 
     */
    public int getNumber() {
        return this.number;
    }
    
    /**
     * @param number
     */
    public void setNumber(int number) {
        this.number = number;
    }
    
    /**
     * @param consensusSequence
     */
    public void setConsensusSequence(DNASequence consensusSequence) {
        this.consensusSequence = consensusSequence;
    }
    
    /**
     * @return CDR3 consensus sequence
     */
    public DNASequence getConsensusSequence() {
        return this.consensusSequence;
    }
            
    /**
     * @return true if heavy chains are empty
     */
    public boolean isEmpty() {
        return heavyChains.isEmpty();
    }
    
    /**
     * @return the heavyChains
     */
    public LinkedHashMap<String, HeavyChain> getHeavyChains() {
        return heavyChains;
    }

    /**
     * @param heavyChains the heavyChains to set
     */
    public void setHeavyChains(LinkedHashMap<String, HeavyChain> heavyChains) {
        this.heavyChains = heavyChains;
    }

    /**
     * @return rearrangement (VJ genes)
     */
    public String getRearrangement() {
        return rearrangement;
    }
    
    /**
     * @param rearrangement (V.J genes)
     */
    public void setRearrangement (String rearrangement) {
        this.rearrangement = rearrangement;
    }

    public boolean hasID(String id) {
        boolean res = false;
        
        id = id.replace("centroid=", "");
        id = id.replace(";", "");
        id = id.replaceAll("seqs=\\d+", "");
        
        if (!heavyChains.isEmpty()) {
            Set set = heavyChains.keySet();
            Iterator<String> it = set.iterator();
            
            while (it.hasNext()) {
                String hcID = it.next();
                res = id.equalsIgnoreCase(hcID);
                
                if (res) {
                    break;
                }
            }
        }
        
        return res;
    }

    public String getName(){
        String name = rearrangement + "." + number;
        
        return name;
    }
}
