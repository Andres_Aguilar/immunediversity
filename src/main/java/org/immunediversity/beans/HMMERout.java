/*
 * Copyright (C) 2015 andres
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.beans;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class HMMERout {
    private String ID;
    private String start;
    private String end;

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return the start
     */
    public String getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * @return the end
     */
    public String getEnd() {
        return end;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * @return Start position as int
     */
    public int getStartAsInt(){
        int startInt;
        try {
            startInt = Integer.parseInt(start);
        } catch(NumberFormatException e) {
            startInt = -1;
        }
        return startInt;
    }
    
    /**
     * @return Start position as int
     */
    public int getEndAsInt(){
        int endInt;
        try {
            endInt = Integer.parseInt(end);
        } catch(NumberFormatException e) {
            endInt = -1;
        }
        return endInt;
    }

    public void print() {
        System.out.println("ID: " + this.ID + "\tStart: " + this.start + "\tEnd: " + this.end);
    }
}
