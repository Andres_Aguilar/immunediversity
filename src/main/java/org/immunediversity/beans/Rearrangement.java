/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.beans;

import java.util.LinkedList;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class Rearrangement {

    private String rearrangement = "";
    private LinkedList<HeavyChain> heavyChains = new LinkedList();

    /**
     * @return 
     */
    public boolean isEmpty() {
        return heavyChains.isEmpty();
    }
    
    /**
     * @return the heavyChains
     */
    public LinkedList<HeavyChain> getHeavyChains() {
        return heavyChains;
    }

    /**
     * @param heavyChains the heavyChains to set
     */
    public void setHeavyChains(LinkedList<HeavyChain> heavyChains) {
        this.heavyChains = heavyChains;
    }

    /**
     * @return rearrangement (VJ genes)
     */
    public String getRearrangement() {
        return rearrangement;
    }
    
    /**
     * @param rearrangement (V.J genes)
     */
    public void setRearrangement (String rearrangement) {
        this.rearrangement = rearrangement;
    }
}
