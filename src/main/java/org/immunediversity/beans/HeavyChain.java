/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.beans;

import org.biojava3.core.sequence.DNASequence;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class HeavyChain {
    private String ID;
    private String Vgene = "";
    private String Dgene = "";
    private String Jgene = "";
    private String alleleV = "";
    private String alleleD = "";
    private String alleleJ = "";
    private DNASequence sequence;
    private DNASequence cdr3;
    private int startV;
    private int endV;
    private int startJ;
    private int endJ;
    private int startFRW1;
    private int endFRW1;
    private int startFRW2;
    private int endFRW2;
    private int startFRW3;
    private int endFRW3;
    private int startCDR1;
    private int endCDR1;
    private int startCDR2;
    private int endCDR2;
    private int startCDR3;
    private int endCDR3;
    private boolean inFrame = false;
    
    /**
     * @param inFrame true if sequence is in frame
     */
    public void setInFrame(boolean inFrame) {
        this.inFrame = inFrame;
    }
    
    /**
     * @return true if sequence is in frame
     */
    public boolean getInFrame() {
        return this.inFrame;
    }
        
    /**
     * @return Rearrangement VGene.JGene
     */
    public String getRearrangement() {
        return Vgene + "." + Jgene;
    }
    
    /**
     * @return Rearrangement VGene.JGene
     */
    public String getRearrangementAllele() {
        return Vgene + "*" + alleleV + "." + Jgene + "*" + alleleJ;
    }
    
    /**
     * @return the ID
     */
    public String getID() {
        return ID.trim();
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return the Vgene
     */
    public String getVgene() {
        return Vgene.trim();
    }

    /**
     * @param Vgene the Vgene to set
     */
    public void setVgene(String Vgene) {
        this.Vgene = Vgene;
    }

    /**
     * @return the Dgene
     */
    public String getDgene() {
        if (Dgene.equalsIgnoreCase("N/A")) {
            return "";
        }
        return Dgene;
    }

    /**
     * @param Dgene the Dgene to set
     */
    public void setDgene(String Dgene) {
        this.Dgene = Dgene;
    }

    /**
     * @return the Jgene
     */
    public String getJgene() {
        return Jgene.trim();
    }

    /**
     * @param Jgene the Jgene to set
     */
    public void setJgene(String Jgene) {
        this.Jgene = Jgene;
    }

    /**
     * @return the alleleV
     */
    public String getAlleleV() {
        return alleleV.trim();
    }

    /**
     * @param alleleV the alleleV to set
     */
    public void setAlleleV(String alleleV) {
        this.alleleV = alleleV;
    }

    /**
     * @return the alleleD
     */
    public String getAlleleD() {
        if (alleleD.equalsIgnoreCase("N/A")) {
            return "";
        }
        return alleleD;
    }

    /**
     * @param alleleD the alleleD to set
     */
    public void setAlleleD(String alleleD) {
        this.alleleD = alleleD;
    }

    /**
     * @return the alleleJ
     */
    public String getAlleleJ() {
        return alleleJ.trim();
    }

    /**
     * @param alleleJ the alleleJ to set
     */
    public void setAlleleJ(String alleleJ) {
        this.alleleJ = alleleJ;
    }

    /**
     * @return the sequence
     */
    public DNASequence getSequence() {
        return sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(DNASequence sequence) {
        this.sequence = sequence;
    }

    /**
     * @return the cdr3
     */
    public DNASequence getCdr3() {
        return cdr3;
    }

    /**
     * @param cdr3 the cdr3 to set
     */
    public void setCdr3(DNASequence cdr3) {
        this.cdr3 = cdr3;
    }

    /**
     * @return the startV
     */
    public int getStartV() {
        return startV;
    }

    /**
     * @param startV the startV to set
     */
    public void setStartV(int startV) {
        this.startV = startV;
    }

    /**
     * @return the endV
     */
    public int getEndV() {
        return endV;
    }

    /**
     * @param endV the endV to set
     */
    public void setEndV(int endV) {
        this.endV = endV;
    }

    /**
     * @return the startJ
     */
    public int getStartJ() {
        return startJ;
    }

    /**
     * @param startJ the startJ to set
     */
    public void setStartJ(int startJ) {
        this.startJ = startJ;
    }

    /**
     * @return the endJ
     */
    public int getEndJ() {
        return endJ;
    }

    /**
     * @param endJ the endJ to set
     */
    public void setEndJ(int endJ) {
        this.endJ = endJ;
    }

    /**
     * @return the startFRW1
     */
    public int getStartFRW1() {
        return startFRW1;
    }

    /**
     * @param startFRW1 the startFRW1 to set
     */
    public void setStartFRW1(int startFRW1) {
        this.startFRW1 = startFRW1;
    }

    /**
     * @return the endFRW1
     */
    public int getEndFRW1() {
        return endFRW1;
    }

    /**
     * @param endFRW1 the endFRW1 to set
     */
    public void setEndFRW1(int endFRW1) {
        this.endFRW1 = endFRW1;
    }

    /**
     * @return the startFRW2
     */
    public int getStartFRW2() {
        return startFRW2;
    }

    /**
     * @param startFRW2 the startFRW2 to set
     */
    public void setStartFRW2(int startFRW2) {
        this.startFRW2 = startFRW2;
    }

    /**
     * @return the endFRW2
     */
    public int getEndFRW2() {
        return endFRW2;
    }

    /**
     * @param endFRW2 the endFRW2 to set
     */
    public void setEndFRW2(int endFRW2) {
        this.endFRW2 = endFRW2;
    }

    /**
     * @return the startFRW3
     */
    public int getStartFRW3() {
        return startFRW3;
    }

    /**
     * @param startFRW3 the startFRW3 to set
     */
    public void setStartFRW3(int startFRW3) {
        this.startFRW3 = startFRW3;
    }

    /**
     * @return the endFRW3
     */
    public int getEndFRW3() {
        return endFRW3;
    }

    /**
     * @param endFRW3 the endFRW3 to set
     */
    public void setEndFRW3(int endFRW3) {
        this.endFRW3 = endFRW3;
    }

    /**
     * @return the startCDR1
     */
    public int getStartCDR1() {
        return startCDR1;
    }

    /**
     * @param startCDR1 the startCDR1 to set
     */
    public void setStartCDR1(int startCDR1) {
        this.startCDR1 = startCDR1;
    }

    /**
     * @return the endCDR1
     */
    public int getEndCDR1() {
        return endCDR1;
    }

    /**
     * @param endCDR1 the endCDR1 to set
     */
    public void setEndCDR1(int endCDR1) {
        this.endCDR1 = endCDR1;
    }

    /**
     * @return the startCDR2
     */
    public int getStartCDR2() {
        return startCDR2;
    }

    /**
     * @param startCDR2 the startCDR2 to set
     */
    public void setStartCDR2(int startCDR2) {
        this.startCDR2 = startCDR2;
    }

    /**
     * @return the endCDR2
     */
    public int getEndCDR2() {
        return endCDR2;
    }

    /**
     * @param endCDR2 the endCDR2 to set
     */
    public void setEndCDR2(int endCDR2) {
        this.endCDR2 = endCDR2;
    }

    /**
     * @return the startCDR3
     */
    public int getStartCDR3() {
        return startCDR3;
    }

    /**
     * @param startCDR3 the startCDR3 to set
     */
    public void setStartCDR3(int startCDR3) {
        this.startCDR3 = startCDR3;
    }

    /**
     * @return the endCDR3
     */
    public int getEndCDR3() {
        return endCDR3;
    }

    /**
     * @param endCDR3 the endCDR3 to set
     */
    public void setEndCDR3(int endCDR3) {
        this.endCDR3 = endCDR3;
    }
    
    public boolean hasVgene() {
        return (this.getStartV() != 0 && this.getEndV() != 0);
    }
    
    public boolean hasDgene() {
        return !(this.getDgene().isEmpty() && this.getAlleleD().isEmpty());
    }
    
    public boolean hasJgene() {
        return (this.getStartJ() != 0 && this.getEndJ() != 0);
    }
    
    public boolean hasCDR3Sequence() {
        return (this.cdr3 != null);
    }
    
    public boolean hasCDR3Coords() {
        return (this.getStartCDR3() != 0 && this.getEndCDR3() != 0);
    }
    
    public boolean hasSequence() {
        return (this.getSequence() != null);
    }
    
    public void print() {
        System.out.println("ID: " + this.getID());
        if (this.hasVgene()) {
            System.out.println("V gene: " + this.getVgene() + "*" + this.getAlleleV() + "\tstart: " + this.getStartV() + "\tend: " + this.getEndV());
        }
        System.out.println("In frame: " + this.inFrame);
        if (this.hasDgene()) {
            System.out.println("D gene: " + this.getDgene() + "*" + this.getAlleleD());
        }
        if (this.hasJgene()) {
            System.out.println("J gene: " + this.getJgene() + "\tstart: " + this.getStartJ() + "\tend: " + this.getEndJ());
        }
        // frameworks and CDRs
        System.out.println("FRW1\tstart: " + this.getStartFRW1() + "\tend: " + this.getEndFRW1());
        System.out.println("CDR1\tstart: " + this.getStartCDR1() + "\tend: " + this.getEndCDR1());
        
        System.out.println("FRW2\tstart: " + this.getStartFRW2() + "\tend: " + this.getEndFRW2());
        System.out.println("CDR2\tstart: " + this.getStartCDR2() + "\tend: " + this.getEndCDR2());
        
        System.out.println("FRW3\tstart: " + this.getStartFRW3() + "\tend: " + this.getEndFRW3());
        if (this.hasCDR3Coords()) {
            System.out.println("CDR3\tstart: " + this.getStartCDR3() + "\tend: " + this.getEndCDR3());
        }
        
        if (this.hasSequence()) {
            System.out.println("VDJ Sequence: " + this.getSequence().getSequenceAsString());
        }
        if (this.hasCDR3Sequence()) {
            System.out.println("CDR3 sequence: " + this.getCdr3().getSequenceAsString());
        }
        System.out.println("*************************************************");
    }
}