/*
 * Copyright (C) 2015 Andres Aguilar <andresyoshimar@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.beans;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class HeavyChain2 {
    private String ID;
    private String vSegment;
    private String dSegment;
    private String jSegment;
    private String sequence;
    
    
    public void print() {
        System.out.println("ID: " + this.ID);
        System.out.println("V Gene: " + this.vSegment);
        System.out.println("D Gene: " + this.dSegment);
        System.out.println("J Gene: " + this.jSegment);
        System.out.println("Sequence: " + this.sequence);
    }
    
    /**
     * @return */
    public String getVAllele() {
        return (this.vSegment.split("\\*")[1]).trim();
    }
    
    /**
     * @return */
    public String getDAllele() {
        return (this.dSegment.split("\\*")[1]).trim();
    }
    
    /**
     * @return */
    public String getJAllele() {
        return (this.jSegment.split("\\*")[1]).trim();
    }
    
    /**
     * @return */
    public String getVgene() {
        return (this.vSegment.split("\\*")[0]).trim();
    }
    
    /**
     * @return */
    public String getDgene() {
        return (this.dSegment.split("\\*")[0]).trim();
    }
    
    /**
     * @return */
    public String getJgene() {
        return (this.jSegment.split("\\*")[0]).trim();
    }
    
    /**
     * @return the ID
     */
    public String getID() {
        return ID.trim();
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return the vSegment
     */
    public String getvSegment() {
        return vSegment.trim();
    }

    /**
     * @param vSegment the vSegment to set
     */
    public void setvSegment(String vSegment) {
        this.vSegment = vSegment;
    }

    /**
     * @return the dSegment
     */
    public String getdSegment() {
        return dSegment.trim();
    }

    /**
     * @param dSegment the dSegment to set
     */
    public void setdSegment(String dSegment) {
        this.dSegment = dSegment;
    }

    /**
     * @return the jSegment
     */
    public String getjSegment() {
        return jSegment.trim();
    }

    /**
     * @param jSegment the jSegment to set
     */
    public void setjSegment(String jSegment) {
        this.jSegment = jSegment;
    }

    /**
     * @return the sequence
     */
    public String getSequence() {
        return sequence.trim();
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public boolean hasVsegment() {
        return this.vSegment != null;
    }
    
    public boolean hasDsegment() {
        return this.dSegment != null;
    }
    
    public boolean hasJsegment() {
        return this.jSegment != null;
    }
}
