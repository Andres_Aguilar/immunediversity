/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.beans;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class IgLamda extends LightChain{
    public void print() {
        System.out.println("ID: " + this.getId());
        System.out.println("V gene: " + this.getvGene() + "\tStart: " + this.getStartV() + "\tEnd: " + this.getEndV());
        System.out.println("J gene: " + this.getjGene() + "\tStart: " + this.getStartJ() + "\tEnd: " + this.getEndJ());
        System.out.println("FRW1\tstart: " + this.getStartFRW1() + "\tend: " + this.getEndFRW1());
        System.out.println("CDR1\tstart: " + this.getStartCDR1() + "\tend: " + this.getEndCDR1());
        System.out.println("FRW2\tstart: " + this.getStartFRW2() + "\tend: " + this.getEndFRW2());
        System.out.println("CDR2\tstart: " + this.getStartCDR2() + "\tend: " + this.getEndCDR2());
        System.out.println("FRW3\tstart: " + this.getStartFRW3() + "\tend: " + this.getEndFRW3());
        System.out.println("CDR3\tstart: " + this.getStartCDR3() + "\tend: " + this.getEndCDR3());
    }
}
