/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.beans;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class ConsensusHeavyChain extends HeavyChain2 {
    private short startFRW1;
    private short startFRW2;
    private short startFRW3;
    private short endFRW1;
    private short endFRW2;
    private short endFRW3;
    private short startCDR1;
    private short startCDR2;
    private short endCDR1;
    private short endCDR2;
    private boolean inFrame;
    private short startCDR3;
    private short endCDR3;
    
    @Override
    public void print() {
        System.out.println("ID: " + super.getID());
        System.out.println("Rearrangement: " + getvSegment() + "." + getdSegment() + "." + getjSegment());
        System.out.println("InFrame: " + isInFrame());
        System.out.println("Sequence: " + getSequence());
    }

    /**
     * @return the startFRW1
     */
    public short getStartFRW1() {
        return startFRW1;
    }

    /**
     * @param startFRW1 the startFRW1 to set
     */
    public void setStartFRW1(short startFRW1) {
        this.startFRW1 = startFRW1;
    }

    /**
     * @return the startFRW2
     */
    public short getStartFRW2() {
        return startFRW2;
    }

    /**
     * @param startFRW2 the startFRW2 to set
     */
    public void setStartFRW2(short startFRW2) {
        this.startFRW2 = startFRW2;
    }

    /**
     * @return the startFRW3
     */
    public short getStartFRW3() {
        return startFRW3;
    }

    /**
     * @param startFRW3 the startFRW3 to set
     */
    public void setStartFRW3(short startFRW3) {
        this.startFRW3 = startFRW3;
    }

    /**
     * @return the endFRW1
     */
    public short getEndFRW1() {
        return endFRW1;
    }

    /**
     * @param endFRW1 the endFRW1 to set
     */
    public void setEndFRW1(short endFRW1) {
        this.endFRW1 = endFRW1;
    }

    /**
     * @return the endFRW2
     */
    public short getEndFRW2() {
        return endFRW2;
    }

    /**
     * @param endFRW2 the endFRW2 to set
     */
    public void setEndFRW2(short endFRW2) {
        this.endFRW2 = endFRW2;
    }

    /**
     * @return the endFRW3
     */
    public short getEndFRW3() {
        return endFRW3;
    }

    /**
     * @param endFRW3 the endFRW3 to set
     */
    public void setEndFRW3(short endFRW3) {
        this.endFRW3 = endFRW3;
    }

    /**
     * @return the startCDR1
     */
    public short getStartCDR1() {
        return startCDR1;
    }

    /**
     * @param startCDR1 the startCDR1 to set
     */
    public void setStartCDR1(short startCDR1) {
        this.startCDR1 = startCDR1;
    }

    /**
     * @return the startCDR2
     */
    public short getStartCDR2() {
        return startCDR2;
    }

    /**
     * @param startCDR2 the startCDR2 to set
     */
    public void setStartCDR2(short startCDR2) {
        this.startCDR2 = startCDR2;
    }

    /**
     * @return the endCDR1
     */
    public short getEndCDR1() {
        return endCDR1;
    }

    /**
     * @param endCDR1 the endCDR1 to set
     */
    public void setEndCDR1(short endCDR1) {
        this.endCDR1 = endCDR1;
    }

    /**
     * @return the endCDR2
     */
    public short getEndCDR2() {
        return endCDR2;
    }

    /**
     * @param endCDR2 the endCDR2 to set
     */
    public void setEndCDR2(short endCDR2) {
        this.endCDR2 = endCDR2;
    }

    /**
     * @return the inFrame
     */
    public boolean isInFrame() {
        return inFrame;
    }

    /**
     * @param inFrame the inFrame to set
     */
    public void setInFrame(boolean inFrame) {
        this.inFrame = inFrame;
    }

    /**
     * @return the startCDR3
     */
    public short getStartCDR3() {
        return startCDR3;
    }

    /**
     * @param startCDR3 the startCDR3 to set
     */
    public void setStartCDR3(short startCDR3) {
        this.startCDR3 = startCDR3;
    }

    /**
     * @return the endCDR3
     */
    public short getEndCDR3() {
        return endCDR3;
    }

    /**
     * @param endCDR3 the endCDR3 to set
     */
    public void setEndCDR3(short endCDR3) {
        this.endCDR3 = endCDR3;
    }

}
