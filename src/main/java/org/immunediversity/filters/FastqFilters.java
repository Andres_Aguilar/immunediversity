/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.filters;

import com.google.common.io.Files;
import com.google.common.io.InputSupplier;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.biojava3.core.sequence.DNASequence;
import org.biojava3.core.sequence.compound.NucleotideCompound;
import org.biojava3.core.sequence.features.FeatureInterface;
import org.biojava3.core.sequence.features.QualityFeature;
import org.biojava3.core.sequence.template.AbstractSequence;
import org.biojava3.sequencing.io.fastq.Fastq;
import org.biojava3.sequencing.io.fastq.FastqReader;
import org.biojava3.sequencing.io.fastq.FastqTools;
import org.biojava3.sequencing.io.fastq.FastqWriter;
import org.biojava3.sequencing.io.fastq.SangerFastqReader;
import org.biojava3.sequencing.io.fastq.SangerFastqWriter;
import org.biojava3.sequencing.io.fastq.StreamListener;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class FastqFilters {
    public List<DNASequence> nFilter(List<DNASequence> sequences) {
        List<DNASequence> goodSequences = new LinkedList<>();

        if (!sequences.isEmpty()) {
            Iterator<DNASequence> iterator = sequences.iterator();

            while (iterator.hasNext()) {
                DNASequence sequence = iterator.next();

                if (!sequence.getSequenceAsString().contains("n") || !sequence.getSequenceAsString().contains("N")) {
                    goodSequences.add(sequence);
                }
            }
        }

        return goodSequences;
    }

    public int[] streamNFilter(File inFile, File outFile) throws IOException {

        FastqReader fastqReader = new SangerFastqReader();
        InputSupplier inputSupplier = Files.newReaderSupplier(inFile,
                Charset.defaultCharset());

        final AtomicInteger totalSequences = new AtomicInteger();
        final AtomicInteger filteredSequences = new AtomicInteger();

        // output
        final FastqWriter fastqWriter = new SangerFastqWriter();
        final FileWriter fileWriter = new FileWriter(outFile);

        fastqReader.stream(inputSupplier, new StreamListener() {
            @Override
            public void fastq(Fastq fastq) {
                String sequence = fastq.getSequence();
                totalSequences.incrementAndGet();

                if (!sequence.contains("n") || !sequence.contains("N")) {
                    try {
                        fastqWriter.append(fileWriter, fastq);
                        filteredSequences.incrementAndGet();
                    } catch (IOException ex) {
                        System.err.println(ex.getMessage());
                    }
                }
            }
        });

        fileWriter.flush();
        fileWriter.close();

        int[] result = new int[2];

        result[0] = totalSequences.get();
        result[1] = filteredSequences.get();

        return result;
    }

    public List<DNASequence> filterBySize(List<DNASequence> sequences,
            final int minSize) {
        List<DNASequence> god_sequences = new LinkedList<>();

        if (sequences.size() > 0) {
            Iterator<DNASequence> iterator = sequences.iterator();

            while (iterator.hasNext()) {
                DNASequence sequence = iterator.next();

                if (sequence.toString().length() >= minSize) {
                    god_sequences.add(sequence);
                }
            }
        }

        return god_sequences;
    }

    @SuppressWarnings("rawtypes")
    public List<DNASequence> filterByQuality(List<DNASequence> sequences,
            final int minQual) {
        List<DNASequence> godQual = new LinkedList<>();

        Iterator<DNASequence> iterator = sequences.iterator();

        while (iterator.hasNext()) {
            DNASequence seq = iterator.next();

            // get sequence quality feature
            List<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> features = seq
                    .getFeatures();
            Iterator<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> it2 = features
                    .iterator();

            int qsum = 0;
            double qmean = 0.0;

            // Obtain quality mean
            if (it2.hasNext()) {
                QualityFeature qual = (QualityFeature) it2.next();
                List qualities = qual.getQualities();

                // quality mean
                Iterator qIt = qualities.iterator();
                while (qIt.hasNext()) {
                    qsum += (int) qIt.next();
                }
                qmean = qsum / qualities.size();
            }

            // Add sequence if qmean is >= minQuality
            if (qmean >= minQual) {
                godQual.add(seq);
            }
        }
        return godQual;
    }

    @SuppressWarnings("rawtypes")
    public List<DNASequence> filterByQualityMedian(List<DNASequence> sequences,
            final int minQual) {
        List<DNASequence> godQual = new LinkedList<>();

        Iterator<DNASequence> iterator = sequences.iterator();

        while (iterator.hasNext()) {
            DNASequence seq = iterator.next();

            // get sequence quality feature
            List<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> features = seq
                    .getFeatures();
            Iterator<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> it2 = features
                    .iterator();

            int median = 0;

            // Obtain quality mean
            if (it2.hasNext()) {
                QualityFeature qual = (QualityFeature) it2.next();
                List qualities = qual.getQualities();

                // quality mean
                Iterator qIt = qualities.iterator();

                int[] qualArray = new int[qualities.size()];
                int count = 0;
                while (qIt.hasNext()) {
                    qualArray[count] = (int) qIt.next();
                    count++;
                }
                Arrays.sort(qualArray);

                if (qualArray.length % 2 == 0) {
                    median = qualArray[qualArray.length / 2];
                } else {
                    median = (qualArray[qualArray.length / 2] + qualArray[(qualArray.length - 1) / 2]) / 2;
                }

            }

            // Add sequence if median is >= minQuality
            if (median >= minQual) {
                godQual.add(seq);
            }
        }
        return godQual;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public int[] streamFilterBySize(File inFile, File outFile, final int minSize)
            throws IOException {
        int[] result = new int[2];
        final AtomicInteger totalCount = new AtomicInteger();
        final AtomicInteger filterCount = new AtomicInteger();

        // Input
        FastqReader fastqReader = new SangerFastqReader();
        InputSupplier inputSupplier = Files.newReaderSupplier(inFile,
                Charset.defaultCharset());

        // output
        final FastqWriter fastqWriter = new SangerFastqWriter();
        final FileWriter fileWriter = new FileWriter(outFile);

        fastqReader.stream(inputSupplier, new StreamListener() {
            @Override
            public void fastq(Fastq fastq) {
                totalCount.incrementAndGet();
                if (fastq.getSequence().length() >= minSize) {
                    try {
                        fastqWriter.append(fileWriter, fastq);
                        filterCount.incrementAndGet();
                    } catch (IOException ex) {
                        System.err.println(ex.getMessage());
                    }
                }
            }
        });

        fileWriter.flush();
        fileWriter.close();

        result[0] = totalCount.get();
        result[1] = filterCount.get();

        return result;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public int[] streamFilterByQuality(File inFile, File outFile,
            final int minQual) throws IOException {
        int[] result = new int[2];
        final AtomicInteger totalCount = new AtomicInteger();
        final AtomicInteger filterCount = new AtomicInteger();

        // Input
        FastqReader fastqReader = new SangerFastqReader();
        InputSupplier inputSupplier = Files.newReaderSupplier(inFile,
                Charset.defaultCharset());

        // output
        final FastqWriter fastqWriter = new SangerFastqWriter();
        final FileWriter fileWriter = new FileWriter(outFile);

        fastqReader.stream(inputSupplier, new StreamListener() {
            @Override
            public void fastq(Fastq fastq) {
                totalCount.incrementAndGet();
                DNASequence sequence = FastqTools
                        .createDNASequenceWithQualityScores(fastq);

                // get sequence quality feature
                List<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> features = sequence
                        .getFeatures();
                Iterator<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> it2 = features
                        .iterator();

                int qsum = 0;
                double qmean = 0.0;

                // Obtain quality mean
                if (it2.hasNext()) {
                    QualityFeature qual = (QualityFeature) it2.next();
                    List qualities = qual.getQualities();

                    // quality mean
                    Iterator qIt = qualities.iterator();
                    while (qIt.hasNext()) {
                        qsum += (int) qIt.next();
                    }
                    qmean = (double)qsum / qualities.size();
                    
                    System.out.println("len: " + qualities.size() + " Sum: " + qsum + " Mean: " + qmean);
                }

                // Add sequence if qmean is >= minQuality
                
                if (qmean >= minQual) {
                    try {
                        fastqWriter.append(fileWriter, fastq);
                        filterCount.incrementAndGet();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        System.err.println(e.getMessage());
                    }
                }
            }
        });

        fileWriter.flush();
        fileWriter.close();

        result[0] = totalCount.get();
        result[1] = filterCount.get();
        
        System.out.println("Total: " + filterCount.get());

        return result;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public int[] streamFilterByQualityMedian(File inFile, File outFile,
            final int minQual) throws IOException {
        int[] result = new int[2];
        final AtomicInteger totalCount = new AtomicInteger();
        final AtomicInteger filterCount = new AtomicInteger();

        // Input
        FastqReader fastqReader = new SangerFastqReader();
        InputSupplier inputSupplier = Files.newReaderSupplier(inFile,
                Charset.defaultCharset());

        // output
        final FastqWriter fastqWriter = new SangerFastqWriter();
        final FileWriter fileWriter = new FileWriter(outFile);

        fastqReader.stream(inputSupplier, new StreamListener() {
            @Override
            public void fastq(Fastq fastq) {
                totalCount.incrementAndGet();
                DNASequence sequence = FastqTools
                        .createDNASequenceWithQualityScores(fastq);

                // get sequence quality feature
                List<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> features = sequence
                        .getFeatures();
                Iterator<FeatureInterface<AbstractSequence<NucleotideCompound>, NucleotideCompound>> it2 = features
                        .iterator();

                int median = 0;

                // Obtain quality mean
                if (it2.hasNext()) {
                    QualityFeature qual = (QualityFeature) it2.next();
                    List qualities = qual.getQualities();

                    // quality mean
                    Iterator qIt = qualities.iterator();

                    int[] qualArray = new int[qualities.size()];
                    int count = 0;

                    while (qIt.hasNext()) {
                        qualArray[count] = (int) qIt.next();
                        count++;
                    }

                    Arrays.sort(qualArray);

                    if (qualArray.length % 2 == 0) {
                        median = qualArray[qualities.size() / 2];
                    } else {
                        median = (qualArray[qualities.size() / 2] + qualArray[(qualities.size() - 1) / 2]) / 2;
                    }
                }

                // Add sequence if qmean is >= minQuality
                
                if (median >= minQual) {
                    try {
                        fastqWriter.append(fileWriter, fastq);
                        filterCount.incrementAndGet();
                    } catch (IOException e) {
                        System.err.println(e.getMessage());
                    }
                }
            }
        });

        fileWriter.flush();
        fileWriter.close();

        result[0] = totalCount.get();
        result[1] = filterCount.get();

        return result;
    }
}
