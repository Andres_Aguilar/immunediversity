/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.parsers;

import com.google.common.io.Files;
import it.unimi.dsi.fastutil.objects.Reference2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import org.immunediversity.beans.HMMERout;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class HmmerParser {
    
    /**
     * 
     * @param output HMMER output (--domtblout)
     * @return LinkedHashMap with all results
     * @throws java.io.IOException
     */
    public Reference2ObjectOpenHashMap<String, HMMERout> parseDomtbloutOutput(File output) throws IOException {
        Reference2ObjectOpenHashMap<String, HMMERout> out = new Reference2ObjectOpenHashMap<>();
        if (output.exists() && output.canRead()) {
            List<String> lines = Files.readLines(output, Charset.defaultCharset());
            
            if (!lines.isEmpty()) {
                Iterator<String> it = lines.iterator();
                
                while (it.hasNext()) {
                    String line = it.next();
                    
                    if (line.startsWith("#")) {
                        continue;
                    }
                    HMMERout outTemp = new HMMERout();
                    
                    String[] blocks = line.split("\\s+");
                    
                    for (int i = 0; i < blocks.length; i++) {
                        
                        if (i == 0) {
                            // ID column
                            outTemp.setID(blocks[i]);
                        } else if (i == 19) {
                            // Start column
                            outTemp.setStart(blocks[i]);
                        } else if (i == 20) {
                            // End column
                            outTemp.setEnd(blocks[i]);
                        }
                        
                    }
                    out.put(outTemp.getID(), outTemp); // set result object 
                }
            }
        }
        //output.delete();
        return out;
    }

    /**
     * @param output HMMER output (--domtblout)
     * @param results LinkedHashMap with all results
     * @return LinkedHashMap with all results (with correct coordinates, end V and start J)
     * @throws java.io.IOException
     */
    public Reference2ObjectOpenHashMap<String, HMMERout> parseDomtbloutOutput2(File output, Reference2ObjectOpenHashMap<String, HMMERout> results) throws IOException {
        Reference2ObjectOpenHashMap<String, HMMERout> out = new Reference2ObjectOpenHashMap<>();
        if (output.exists() && output.canRead()) {
            List<String> lines = Files.readLines(output, Charset.defaultCharset());
            
            if (!lines.isEmpty()) {
                Iterator<String> it = lines.iterator();
                
                while (it.hasNext()) {
                    String line = it.next();
                    
                    if (line.startsWith("#")) {
                        continue;
                    }
                    HMMERout outTemp = new HMMERout();
                    
                    String[] blocks = line.split("\\s+");
                    
                    for (int i = 0; i < blocks.length; i++) {
                        
                        if (i == 0) {
                            // ID column
                            outTemp.setID(blocks[i]);
                        } else if (i == 19) {
                            // Start column
                            outTemp.setStart(blocks[i]);
                        } else if (i == 20) {
                            // End column
                            outTemp.setEnd(blocks[i]);
                        }
                        
                    }
                    out.put(outTemp.getID(), outTemp); // set result object 
                }
            }
        }
        
        return correctCoordinates(results, out);
    }

    private Reference2ObjectOpenHashMap<String, HMMERout> correctCoordinates(Reference2ObjectOpenHashMap<String, HMMERout> resultsV, Reference2ObjectOpenHashMap<String, HMMERout> resultsJ) {
        Reference2ObjectOpenHashMap<String, HMMERout> results = new Reference2ObjectOpenHashMap<>();
        
        if (!resultsV.isEmpty() && !resultsJ.isEmpty()) {
            Set set = resultsV.keySet();
            Iterator<String> it = set.iterator();
            
            while (it.hasNext()) {
                String id = it.next();
                
                HMMERout v = resultsV.get(id);
                HMMERout j = resultsJ.get(id);
                
                if (v != null && j != null) {
                    HMMERout temp = new HMMERout();
                    
                    temp.setID(id);
                    temp.setStart(v.getEnd());
                    temp.setEnd(j.getStart());
                    
                    results.put(id, temp);
                }
            }
        }
        
        return results;
    }
}
