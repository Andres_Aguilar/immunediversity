/*
 * Copyright (C) 2015 Andres Aguilar <andresyoshimar@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.parsers;

import com.google.common.io.Files;
import it.unimi.dsi.fastutil.objects.Reference2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.immunediversity.beans.BlastCoords;
import org.immunediversity.beans.HeavyChain;
import org.immunediversity.beans.HeavyChain2;
import org.immunediversity.beans.IgKappa;
import org.immunediversity.beans.IgLamda;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class IgBlastParser {

    /**
     * @param blastResults
     * @return 
     */
    public LinkedHashMap<String, IgKappa> parseBlastOutput4Kappa(File blastResults) {
        boolean v = false, j = false;
        LinkedHashMap<String, IgKappa> IgKappaList = new LinkedHashMap<>();

        try {
            List<String> lines = Files.readLines(blastResults, Charset.defaultCharset());

            Iterator<String> it = lines.iterator();
            IgKappa igk = null;

            while (it.hasNext()) {
                String line = it.next();

                // Initialize variables
                if (line.startsWith("# IGBLASTN")) {
                    v = j = false;
                    igk = new IgKappa();

                }

                if (line.startsWith("# Query:")) {
                    String[] idTemp = line.split(":");
                    igk.setId(idTemp[1].trim());
                }
                /*
                 For Frameworks and CDRs
                 Alignment summary between query and top germline V gene hit (from, to, length, matches, mismatches, gaps, percent identity) 
                 */

                // Framework 1
                if (line.startsWith("FWR1")) {
                    String[] frw1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw1Temp[1]);
                        end = Integer.parseInt(frw1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igk.setStartFRW1(start);
                    igk.setEndFRW1(end);
                }
                if (line.startsWith("CDR1")) {
                    String[] cdr1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr1Temp[1]);
                        end = Integer.parseInt(cdr1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igk.setStartCDR1(start);
                    igk.setEndCDR1(end);
                }
                // Framework 2
                if (line.startsWith("FWR2")) {
                    String[] frw2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw2Temp[1]);
                        end = Integer.parseInt(frw2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igk.setStartFRW2(start);
                    igk.setEndFRW2(end);
                }
                if (line.startsWith("CDR2")) {
                    String[] cdr2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr2Temp[1]);
                        end = Integer.parseInt(cdr2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igk.setStartCDR2(start);
                    igk.setEndCDR2(end);
                }
                // Framework 3
                if (line.startsWith("FWR3")) {
                    String[] frw3Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw3Temp[1]);
                        end = Integer.parseInt(frw3Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igk.setStartFRW3(start);
                    igk.setEndFRW3(end);
                }

                if (!v) {
                    if (line.startsWith("V")) {
                        String[] vTemp = line.split("\t");
                        igk.setvGene(vTemp[2]);

                        int start, end;
                        try {
                            start = Integer.parseInt(vTemp[7]);
                            end = Integer.parseInt(vTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }

                        igk.setStartV(start);
                        igk.setEndV(end);

                        v = true;
                    }
                }
                if (!j) {
                    if (line.startsWith("J")) {
                        String[] jTemp = line.split("\t");
                        igk.setjGene(jTemp[2]);

                        int start, end;
                        try {
                            start = Integer.parseInt(jTemp[7]);
                            end = Integer.parseInt(jTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }

                        igk.setStartJ(start);
                        igk.setEndJ(end);

                        j = true;
                        IgKappaList.put(igk.getId(), igk);
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(IgBlastParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return IgKappaList;
    }
    
    /**
     * 
     * @param blastResults
     * @return 
     */
    public LinkedHashMap<String, IgLamda> parseBlastOutput4Lamda(File blastResults) {
        boolean v = false, j = false;
        LinkedHashMap<String, IgLamda> IgLamdaList = new LinkedHashMap<>();

        try {
            List<String> lines = Files.readLines(blastResults, Charset.defaultCharset());

            Iterator<String> it = lines.iterator();
            IgLamda igL = null;

            while (it.hasNext()) {
                String line = it.next();

                // Initialize variables
                if (line.startsWith("# IGBLASTN")) {
                    v = j = false;
                    igL = new IgLamda();

                }

                if (line.startsWith("# Query:")) {
                    String[] idTemp = line.split(":");
                    igL.setId(idTemp[1].trim());
                    //System.out.println("ID: " + igk.getId());
                }
                /*
                 Format for Frameworks and CDRs
                 Alignment summary between query and top germline V gene hit (from, to, length, matches, mismatches, gaps, percent identity) 
                 */

                // Framework 1
                if (line.startsWith("FWR1")) {
                    String[] frw1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw1Temp[1]);
                        end = Integer.parseInt(frw1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igL.setStartFRW1(start);
                    igL.setEndFRW1(end);
                }
                if (line.startsWith("CDR1")) {
                    String[] cdr1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr1Temp[1]);
                        end = Integer.parseInt(cdr1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igL.setStartCDR1(start);
                    igL.setEndCDR1(end);
                }
                // Framework 2
                if (line.startsWith("FWR2")) {
                    String[] frw2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw2Temp[1]);
                        end = Integer.parseInt(frw2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igL.setStartFRW2(start);
                    igL.setEndFRW2(end);
                }
                if (line.startsWith("CDR2")) {
                    String[] cdr2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr2Temp[1]);
                        end = Integer.parseInt(cdr2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igL.setStartCDR2(start);
                    igL.setEndCDR2(end);
                }
                // Framework 3
                if (line.startsWith("FWR3")) {
                    String[] frw3Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw3Temp[1]);
                        end = Integer.parseInt(frw3Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }

                    igL.setStartFRW3(start);
                    igL.setEndFRW3(end);
                }

                if (!v) {
                    if (line.startsWith("V")) {
                        String[] vTemp = line.split("\t");
                        igL.setvGene(vTemp[2]);

                        int start, end;
                        try {
                            start = Integer.parseInt(vTemp[7]);
                            end = Integer.parseInt(vTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }

                        igL.setStartV(start);
                        igL.setEndV(end);

                        v = true;
                    }
                }
                if (!j) {
                    if (line.startsWith("J")) {
                        String[] jTemp = line.split("\t");
                        igL.setjGene(jTemp[2]);

                        int start, end;
                        try {
                            start = Integer.parseInt(jTemp[7]);
                            end = Integer.parseInt(jTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }

                        igL.setStartJ(start);
                        igL.setEndJ(end);

                        j = true;
                        IgLamdaList.put(igL.getId(), igL);
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(IgBlastParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return IgLamdaList;
    }
    
    /**
     * 
     * @param blastResults
     * @return 
     */
    public LinkedHashMap<String, HeavyChain> parseBlastOutput4HeavyChain(File blastResults) {
        boolean v = false, d = false, j = false;
        LinkedHashMap<String, HeavyChain> heavyChainList = new LinkedHashMap<>();
        
        List<String> lines;
        try {
            lines = Files.readLines(blastResults, Charset.defaultCharset());
            
            Iterator<String> it = lines.iterator();
            HeavyChain hc = null;
            
            while (it.hasNext()) {
                String line = it.next();

                // Initialize variables
                if (line.startsWith("# IGBLASTN")) {
                    v = d = j = false;
                    hc = new HeavyChain();
                }
                if (line.startsWith("# Query:")) {
                    String[] idTemp = line.split(":");
                    hc.setID(idTemp[1]);
                }
                /*
                 Format for Frameworks and CDRs
                 Alignment summary between query and top germline V gene hit (from, to, length, matches, mismatches, gaps, percent identity) 
                 */

                // Framework 1
                if (line.startsWith("FWR1")) {
                    String[] frw1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw1Temp[1]);
                        end = Integer.parseInt(frw1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartFRW1(start);
                    hc.setEndFRW1(end);
                }
                if (line.startsWith("CDR1")) {
                    String[] cdr1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr1Temp[1]);
                        end = Integer.parseInt(cdr1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartCDR1(start);
                    hc.setEndCDR1(end);
                }
                if (line.startsWith("FWR2")) {
                    String[] frw2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw2Temp[1]);
                        end = Integer.parseInt(frw2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartFRW2(start);
                    hc.setEndFRW2(end);
                }
                if (line.startsWith("CDR2")) {
                    String[] cdr2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr2Temp[1]);
                        end = Integer.parseInt(cdr2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartCDR2(start);
                    hc.setEndCDR2(end);
                }
                if (line.startsWith("FWR3")) {
                    String[] frw3Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw3Temp[1]);
                        end = Integer.parseInt(frw3Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartFRW3(start);
                    hc.setEndFRW3(end);
                }
                /* VDJ segments */
                if (!v) {
                    if (line.startsWith("V")) {
                        String[] vTemp = line.split("\t");
                        
                        //vTemp[2] has Gene*allele
                        String[] geneAndAllele = vTemp[2].split("\\*");
                        hc.setVgene(geneAndAllele[0]);
                        hc.setAlleleV(geneAndAllele[1]);

                        int start, end;
                        try {
                            start = Integer.parseInt(vTemp[7]);
                            end = Integer.parseInt(vTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }
                        hc.setStartV(start);
                        hc.setEndV(end);

                        v = true;
                    }
                }
                if (!d) {
                    if (line.startsWith("D")) {
                        String[] dTemp = line.split("\t");
                        
                        //dTemp[2] has Gene*allele
                        String[] geneAndAllele = dTemp[2].split("\\*");
                        hc.setDgene(geneAndAllele[0]);
                        hc.setAlleleD(geneAndAllele[1]);

                        int start, end;
                        try {
                            start = Integer.parseInt(dTemp[7]);
                            end = Integer.parseInt(dTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }
                        d = true;
                    }
                }
                if (!j) {
                    if (line.startsWith("J")) {
                        String[] jTemp = line.split("\t");
                        
                        // jTemp has gene*allele
                        String[] geneAndAllele = jTemp[2].split("\\*");
                        hc.setJgene(geneAndAllele[0]);
                        hc.setAlleleJ(geneAndAllele[1]);

                        int start, end;
                        try {
                            start = Integer.parseInt(jTemp[7]);
                            end = Integer.parseInt(jTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }
                        hc.setStartJ(start);
                        hc.setEndJ(end);

                        j = true;
                        heavyChainList.put(hc.getID(), hc);
                    }
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(IgBlastParser.class.getName()).log(Level.SEVERE, null, ex);
        }   

        return heavyChainList;
    }

    /**
     * @param blastResults
     * @return 
     */
    public LinkedHashMap<String, HeavyChain> parseBlastOutput4ConsensusHeavyChain(File blastResults) {
        boolean v = false, d = false, j = false;
        LinkedHashMap<String, HeavyChain> heavyChainList = new LinkedHashMap<>();
        
        List<String> lines;
        try {
            lines = Files.readLines(blastResults, Charset.defaultCharset());
            
            Iterator<String> it = lines.iterator();
            HeavyChain hc = null;
            
            while (it.hasNext()) {
                String line = it.next();

                // Initialize variables
                if (line.startsWith("# IGBLASTN")) {
                    v = d = j = false;
                    hc = new HeavyChain();
                }
                if (line.startsWith("# Query:")) {
                    String[] idTemp = line.split(":");
                    hc.setID(idTemp[1]);
                }
                
                if (line.contains("In-frame")) {
                    hc.setInFrame(true);
                }
                /*
                 Format for Frameworks and CDRs
                 Alignment summary between query and top germline V gene hit (from, to, length, matches, mismatches, gaps, percent identity) 
                 */

                // Framework 1
                if (line.startsWith("FWR1")) {
                    String[] frw1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw1Temp[1]);
                        end = Integer.parseInt(frw1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartFRW1(start);
                    hc.setEndFRW1(end);
                }
                if (line.startsWith("CDR1")) {
                    String[] cdr1Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr1Temp[1]);
                        end = Integer.parseInt(cdr1Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartCDR1(start);
                    hc.setEndCDR1(end);
                }
                if (line.startsWith("FWR2")) {
                    String[] frw2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw2Temp[1]);
                        end = Integer.parseInt(frw2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartFRW2(start);
                    hc.setEndFRW2(end);
                }
                if (line.startsWith("CDR2")) {
                    String[] cdr2Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(cdr2Temp[1]);
                        end = Integer.parseInt(cdr2Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartCDR2(start);
                    hc.setEndCDR2(end);
                }
                if (line.startsWith("FWR3")) {
                    String[] frw3Temp = line.split("\t");

                    int start, end;
                    try {
                        start = Integer.parseInt(frw3Temp[1]);
                        end = Integer.parseInt(frw3Temp[2]);
                    } catch (NumberFormatException e) {
                        start = end = 0;
                    }
                    hc.setStartFRW3(start);
                    hc.setEndFRW3(end);
                }
                /* VDJ segments */
                if (!v) {
                    if (line.startsWith("V")) {
                        String[] vTemp = line.split("\t");
                        
                        //vTemp[2] has Gene*allele
                        String[] geneAndAllele = vTemp[2].split("\\*");
                        hc.setVgene(geneAndAllele[0]);
                        hc.setAlleleV(geneAndAllele[1]);

                        int start, end;
                        try {
                            start = Integer.parseInt(vTemp[7]);
                            end = Integer.parseInt(vTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }
                        hc.setStartV(start);
                        hc.setEndV(end);

                        v = true;
                    }
                }
                if (!d) {
                    if (line.startsWith("D")) {
                        String[] dTemp = line.split("\t");
                        
                        //dTemp[2] has Gene*allele
                        String[] geneAndAllele = dTemp[2].split("\\*");
                        hc.setDgene(geneAndAllele[0]);
                        hc.setAlleleD(geneAndAllele[1]);

                        int start, end;
                        try {
                            start = Integer.parseInt(dTemp[7]);
                            end = Integer.parseInt(dTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }
                        d = true;
                    }
                }
                if (!j) {
                    if (line.startsWith("J")) {
                        String[] jTemp = line.split("\t");
                        
                        // jTemp has gene*allele
                        String[] geneAndAllele = jTemp[2].split("\\*");
                        hc.setJgene(geneAndAllele[0]);
                        hc.setAlleleJ(geneAndAllele[1]);

                        int start, end;
                        try {
                            start = Integer.parseInt(jTemp[7]);
                            end = Integer.parseInt(jTemp[8]);
                        } catch (NumberFormatException e) {
                            start = end = 0;
                        }
                        hc.setStartJ(start);
                        hc.setEndJ(end);

                        j = true;
                        heavyChainList.put(hc.getID(), hc);
                    }
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(IgBlastParser.class.getName()).log(Level.SEVERE, null, ex);
        }   

        return heavyChainList;
    }

    public Reference2ObjectOpenHashMap<String, BlastCoords> parseBlastOutGetCoords(File blastResults) {
        Reference2ObjectOpenHashMap<String, BlastCoords> heavyChainList = new Reference2ObjectOpenHashMap<>();
        
        boolean v = false, j = false;

        List<String> lines;
        try {
            lines = Files.readLines(blastResults, Charset.defaultCharset());
            
            Iterator<String> it = lines.iterator();
            BlastCoords hc = null;
            
            while (it.hasNext()) {
                String line = it.next();

                // Initialize variables
                if (line.startsWith("# IGBLASTN")) {
                    v =  j = false;
                    hc = new BlastCoords();
                }
                if (line.startsWith("# Query:")) {
                    String[] idTemp = line.split(":");
                    hc.setId(idTemp[1]);
                } 
                
                /* VDJ segments */
                if (!v) {
                    if (line.startsWith("V")) {
                        String[] vTemp = line.split("\t");
                        
                        int start;
                        try {
                            start = Integer.parseInt(vTemp[7]);
                        } catch (NumberFormatException e) {
                            start = 0;
                        }
                        hc.setvCoord(start);

                        v = true;
                    }
                }
                if (!j) {
                    if (line.startsWith("J")) {
                        String[] jTemp = line.split("\t");
                        
                        int end;
                        try {
                            
                            end = Integer.parseInt(jTemp[8]);
                        } catch (NumberFormatException e) {
                            end = 0;
                        }
                        hc.setjCoord(end);

                        j = true;
                        heavyChainList.put(hc.getId(), hc);
                    }
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(IgBlastParser.class.getName()).log(Level.SEVERE, null, ex);
        }           
        
        return heavyChainList;
    }

    /**
     * @param blastResults
     * @return 
     */
    public Reference2ObjectOpenHashMap parseBlastOutHeavyChain(File blastResults) {
        boolean v = false, d = false, j = false;
        Reference2ObjectOpenHashMap<String, HeavyChain2> heavyChainList = new Reference2ObjectOpenHashMap<>();
        
        List<String> lines;
        try {
            lines = Files.readLines(blastResults, Charset.defaultCharset());
            
            Iterator<String> it = lines.iterator();
            HeavyChain2 hc = null;
            
            while (it.hasNext()) {
                String line = it.next();

                // Initialize variables
                if (line.startsWith("# IGBLASTN")) {
                    v = d = j = false;
                    hc = new HeavyChain2();
                }
                if (line.startsWith("# Query:")) {
                    String[] idTemp = line.split(":");
                    hc.setID(idTemp[1]);
                }
                /* VDJ segments */
                if (!v) {
                    if (line.startsWith("V")) {
                        String[] vTemp = line.split("\t");
                        
                        //vTemp[2] has Gene*allele
                        hc.setvSegment(vTemp[2]);
                        v = true;
                    }
                }
                if (!d) {
                    if (line.startsWith("D")) {
                        String[] dTemp = line.split("\t");
                        
                        //dTemp[2] has Gene*allele
                         hc.setdSegment(dTemp[2]);

                        d = true;
                    }
                }
                if (!j) {
                    if (line.startsWith("J")) {
                        String[] jTemp = line.split("\t");
                        
                        // jTemp has gene*allele
                        hc.setjSegment(jTemp[2]);
                        
                        j = true;
                        heavyChainList.put(hc.getID(), hc);
                    }
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(IgBlastParser.class.getName()).log(Level.SEVERE, null, ex);
        }   
        
        return heavyChainList;
    }
}