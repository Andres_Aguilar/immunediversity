/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.parsers;

import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import org.biojava3.core.sequence.DNASequence;
import org.immunediversity.IO.Readers;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class UsearchParser {

    /**
     *
     * @param results
     * @return
     * @throws java.io.IOException
     */
    public LinkedHashMap<String, Integer> parseResults(File results) throws IOException {
        LinkedHashMap<String, Integer> groups = new LinkedHashMap();

        if (results.exists() && results.canRead()) {

            List<String> lines = Files.readLines(results, Charset.defaultCharset());

            if (!lines.isEmpty()) {
                Iterator<String> it = lines.iterator();

                while (it.hasNext()) {
                    String line = it.next();

                    if (line.startsWith("C")) {
                        continue;
                    }

                    String[] tab = line.split("\t");
                    groups.put(tab[8], new Integer(tab[1]));
                }
            }
        }
        return groups;
    }

    /**
     * @param consensusFile
     * @return
     * @throws java.io.IOException
     */
    public LinkedHashMap<String, DNASequence> readConsensus(File consensusFile) throws IOException {
        LinkedHashMap<String, DNASequence> consensus = new LinkedHashMap();
        if (consensusFile.exists() && consensusFile.canRead()) {
            Readers read = new Readers();

            consensus = read.readDNAFasta(consensusFile);
        }
        return consensus;
    }

}
