/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.biojava3.core.sequence.DNASequence;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class Subsample {
    /**
     * @param elements List of DNASequences
     * @param sample Number of sequences to pick
     * @return  List with N sequences
     */
    public List<DNASequence> fastqListSubsample(List elements, int sample) {
        RandomDataGenerator rand = new RandomDataGenerator();
        List<DNASequence> sequences = new LinkedList<>();

        if (elements.size() > 0 && elements.size() >= sample) {
            Object[] el = rand.nextSample(elements, sample);

            for (Object el1 : el) {
                sequences.add((DNASequence) el1);
            }
        } else {
            System.err.println("ERROR: Subsample - sample is greater tan sequences number or sequences length is 0!!!");
        }

        return sequences;
    }

    /**
     * @param elements List of DNASequences
     * @param sample Number of sequences to pick
     * @return  List with N sequences
     */
    public List<DNASequence> fastaListSubsample(List elements, int sample) {
        RandomDataGenerator rand = new RandomDataGenerator();
        List<DNASequence> sequences = new LinkedList<>();

        if (elements.size() > 0 && elements.size() >= sample) {
            Object[] el = rand.nextSample(elements, sample);

            for (Object el1 : el) {
                sequences.add((DNASequence) el1);
            }
        }

        return sequences;
    }

    /**
     * @param elements List of DNASequences
     * @param sample Number of sequences to pick
     * @return  LinkedHashMap with N sequences
     */
    public LinkedHashMap<String, DNASequence> fastaHashMapSubsample(LinkedHashMap elements, int sample) {
        RandomDataGenerator rand = new RandomDataGenerator();
        List<DNASequence> sequences = new LinkedList<>();
        List<DNASequence> sequences2 = new LinkedList<>();
        LinkedHashMap<String, DNASequence> subset = new LinkedHashMap<>();

        Set set = elements.keySet();
        if (!set.isEmpty()) {

            // LinkedHashMap to LinkedList
            Iterator<DNASequence> iterator = set.iterator();

            while (iterator.hasNext()) {
                sequences.add(iterator.next());
            }

            // subset
            if (sequences.size() > 0 && sequences.size() >= sample) {
                Object[] el = rand.nextSample(sequences, sample);

                for (Object el1 : el) {
                    sequences2.add((DNASequence) elements.get(el1));
                }
                
                // cleaning objects
                sequences.clear();

                // LinkedList to LinkedHashMap 
                if (sequences2.size() > 0) {
                    Iterator<DNASequence> it = sequences2.iterator();

                    while (it.hasNext()) {
                        DNASequence seq = it.next();

                        subset.put(seq.getOriginalHeader(), seq);
                    }
                    
                    // cleaning objects
                    sequences2.clear();  
                }
            }
        }

        return subset;
    }
}
