/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.util;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class PlatformUtils {
    private static String getOsName() {
        return System.getProperty("os.name");
    }

    public static boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

    public static boolean isLinux() {
        return getOsName().startsWith("Linux");
    }
    
    public static boolean isMac() {
        return getOsName().startsWith("Darwin");
    }
    
    private static boolean is64bits() {
        return System.getProperty("os.arch").equalsIgnoreCase("amd64");
    }
    
    /**
     * Simple way to get the platform separator character
     * @return String platform separator (/ for *nix like systems, \ for Windows )
     */
    public static String getSeparator(){
        return System.getProperty("file.separator");
    }
}
