/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.util;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.biojava3.core.sequence.DNASequence;
import org.immunediversity.beans.HeavyChain;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class ObjectSequenceConverter {

    public List<DNASequence> LinkedHashMap2List(LinkedHashMap<String, DNASequence> elements) {
        List<DNASequence> sequences = new LinkedList<>();

        Set set = elements.keySet();
        if (!set.isEmpty()) {

            // LinkedHashMap to LinkedList
            Iterator<String> iterator = set.iterator();

            while (iterator.hasNext()) {
                DNASequence seq = (DNASequence) elements.get(iterator.next());
                sequences.add(seq);
            }
        }
        elements.clear();
        return sequences;
    }

    public LinkedHashMap<String, DNASequence> List2LinkedHashMap(List<DNASequence> elements) {
        LinkedHashMap<String, DNASequence> sequences = new LinkedHashMap<>();

        // LinkedList to LinkedHashMap 
        if (elements.size() > 0) {
            Iterator<DNASequence> it = elements.iterator();

            while (it.hasNext()) {
                DNASequence seq = it.next();

                sequences.put(seq.getOriginalHeader(), seq);
            }
        }

        elements.clear();
        return sequences;
    }

    public LinkedHashMap<String, HeavyChain> List2LinkedHashMap(LinkedList<HeavyChain> elements) {
        LinkedHashMap<String, HeavyChain> heavyChains = new LinkedHashMap();
        
        if (!elements.isEmpty()) {
            Iterator<HeavyChain> it = elements.iterator();
            
            while (it.hasNext()) {
                HeavyChain hc = it.next();
                
                heavyChains.put(hc.getID(), hc);
            }
        }
        
        elements.clear();
        return heavyChains;
    }
}
