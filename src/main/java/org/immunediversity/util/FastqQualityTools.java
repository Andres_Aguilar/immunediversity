/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.util;

import java.util.LinkedHashMap;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class FastqQualityTools {
    public static LinkedHashMap<String, String> sanger(){
        LinkedHashMap<String, String> quality = new LinkedHashMap<>(); 
        
        // ! " # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I
        
        quality.put("0", Character.toString('!'));
        quality.put("1", Character.toString('"'));
        quality.put("2", Character.toString('#'));
        quality.put("3", Character.toString('$'));
        quality.put("4", Character.toString('%'));
        quality.put("5", Character.toString('&'));
        quality.put("6", Character.toString('\''));
        quality.put("7", Character.toString('('));
        quality.put("8", Character.toString(')'));
        quality.put("9", Character.toString('*'));
        
        quality.put("10", Character.toString('+'));
        quality.put("11", Character.toString(','));
        quality.put("12", Character.toString('-'));
        quality.put("13", Character.toString('.'));
        quality.put("14", Character.toString('/'));
        quality.put("15", Character.toString('0'));
        quality.put("16", Character.toString('1'));
        quality.put("17", Character.toString('2'));
        quality.put("18", Character.toString('3'));
        quality.put("19", Character.toString('4'));
        
        // ! " # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I
        
        quality.put("20", Character.toString('5'));
        quality.put("21", Character.toString('6'));
        quality.put("22", Character.toString('7'));
        quality.put("23", Character.toString('8'));
        quality.put("24", Character.toString('9'));
        quality.put("25", Character.toString(':'));
        quality.put("26", Character.toString(';'));
        quality.put("27", Character.toString('<'));
        quality.put("28", Character.toString('='));
        quality.put("29", Character.toString('>'));
        
        quality.put("30", Character.toString('?'));
        quality.put("31", Character.toString('@'));
        quality.put("32", Character.toString('A'));
        quality.put("33", Character.toString('B'));
        quality.put("34", Character.toString('C'));
        quality.put("35", Character.toString('D'));
        quality.put("36", Character.toString('E'));
        quality.put("37", Character.toString('F'));
        quality.put("38", Character.toString('G'));
        quality.put("39", Character.toString('H'));
        
        quality.put("40", Character.toString('I'));
        
        return quality;
                
    }

    public static LinkedHashMap<String, String> illumina18(){
        LinkedHashMap<String, String> quality = new LinkedHashMap<>(); 
        
        // ! " # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J
        quality.put("0", Character.toString('!'));
        quality.put("1", Character.toString('"'));
        quality.put("2", Character.toString('#'));
        quality.put("3", Character.toString('$'));
        quality.put("4", Character.toString('%'));
        quality.put("5", Character.toString('&'));
        quality.put("6", Character.toString('\''));
        quality.put("7", Character.toString('('));
        quality.put("8", Character.toString(')'));
        quality.put("9", Character.toString('*'));
        
        quality.put("10", Character.toString('+'));
        quality.put("11", Character.toString(','));
        quality.put("12", Character.toString('-'));
        quality.put("13", Character.toString('.'));
        quality.put("14", Character.toString('/'));
        quality.put("15", Character.toString('0'));
        quality.put("16", Character.toString('1'));
        quality.put("17", Character.toString('2'));
        quality.put("18", Character.toString('3'));
        quality.put("19", Character.toString('4'));
        
        quality.put("20", Character.toString('5'));
        quality.put("21", Character.toString('6'));
        quality.put("22", Character.toString('7'));
        quality.put("23", Character.toString('8'));
        quality.put("24", Character.toString('9'));
        quality.put("25", Character.toString(':'));
        quality.put("26", Character.toString(';'));
        quality.put("27", Character.toString('<'));
        quality.put("28", Character.toString('='));
        quality.put("29", Character.toString('>'));
        
        quality.put("30", Character.toString('?'));
        quality.put("31", Character.toString('@'));
        quality.put("32", Character.toString('A'));
        quality.put("33", Character.toString('B'));
        quality.put("34", Character.toString('C'));
        quality.put("35", Character.toString('D'));
        quality.put("36", Character.toString('E'));
        quality.put("37", Character.toString('F'));
        quality.put("38", Character.toString('G'));
        quality.put("39", Character.toString('H'));
        
        quality.put("40", Character.toString('I'));
        quality.put("41", Character.toString('J'));
        
        return quality;
    }
    
    public static LinkedHashMap<String, String> solexa(){
        LinkedHashMap<String, String> quality = new LinkedHashMap<>(); 
        
        // ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _ ` a b c d e f g h
        quality.put("-5", Character.toString(';'));
        quality.put("-4", Character.toString('<'));
        quality.put("-3", Character.toString('='));
        quality.put("-2", Character.toString('>'));
        quality.put("-1", Character.toString('?'));
        quality.put("0", Character.toString('@'));
        quality.put("1", Character.toString('A'));
        quality.put("2", Character.toString('B'));
        quality.put("3", Character.toString('C'));
        quality.put("4", Character.toString('D'));
        quality.put("5", Character.toString('E'));
        quality.put("6", Character.toString('F'));
        quality.put("7", Character.toString('G'));
        quality.put("8", Character.toString('H'));
        quality.put("9", Character.toString('I'));
        quality.put("10", Character.toString('J'));
        quality.put("11", Character.toString('K'));
        quality.put("12", Character.toString('L'));
        quality.put("13", Character.toString('M'));
        quality.put("14", Character.toString('N'));
        quality.put("15", Character.toString('O'));
        quality.put("16", Character.toString('P'));
        quality.put("17", Character.toString('Q'));
        quality.put("18", Character.toString('R'));
        quality.put("19", Character.toString('S'));
        quality.put("20", Character.toString('T'));
        
        // ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _ ` a b c d e f g h
        quality.put("21", Character.toString('U'));
        quality.put("22", Character.toString('V'));
        quality.put("23", Character.toString('W'));
        quality.put("24", Character.toString('X'));
        quality.put("25", Character.toString('Y'));
        quality.put("26", Character.toString('Z'));
        quality.put("27", Character.toString('['));
        quality.put("28", Character.toString('\\'));
        quality.put("29", Character.toString(']'));
        quality.put("30", Character.toString('^'));
        quality.put("31", Character.toString('_'));
        quality.put("32", Character.toString('`'));
        quality.put("33", Character.toString('a'));
        quality.put("34", Character.toString('b'));
        quality.put("35", Character.toString('c'));
        quality.put("36", Character.toString('d'));
        quality.put("37", Character.toString('e'));
        quality.put("38", Character.toString('f'));
        quality.put("39", Character.toString('g'));
        quality.put("40", Character.toString('h'));
        
        return quality;
    }
}
