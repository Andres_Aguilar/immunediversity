/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.executors;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.immunediversity.util.PlatformUtils;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class Usearch implements Runnable { //

    private Properties prop = null;
    private Namespace params = null;
    private File tempFile, resultFile, consensusFile;
    private double id = 0.0, qfract = 0.0, tfract = 0.0;

    /**
     * @param prop the prop to set
     */
    public void setProp(Properties prop) {
        this.prop = prop;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Namespace params) {
        this.params = params;
    }

    /**
     * @param tempFile the tempFile to set
     */
    public void setFastaFile(File tempFile) {
        this.tempFile = tempFile;
    }

    /**
     * @param resultFile the resultFile to set
     */
    public void setResultFile(File resultFile) {
        this.resultFile = resultFile;
    }

    /**
     * @param consensusFile the consensusFile to set
     */
    public void setConsensusFile(File consensusFile) {
        this.consensusFile = consensusFile;
    }

    public void setID(double id) {
        this.id = id;
    }
    
    public void setTfract(double tfract) {
        this.tfract = tfract;
    }
    
    public void setQfract(double qfract) {
        this.qfract = qfract;
    }
    
    public String makeCommand() {
        String cmd = "";
        
        if (tempFile != null && resultFile != null && consensusFile != null && prop != null && params != null) { 

            cmd += " -cluster_fast " + tempFile.toString();
            if (id != 0.0) {
                cmd += " -id " + id;
            } else {
                cmd += " -id " + params.getDouble("id");
            }
            if (qfract != 0.0 && tfract != 0.0) {
                cmd += " -query_cov " + qfract + " -target_cov " + tfract;
            } else {
                cmd += " -query_cov 0.97 -target_cov 0.97 ";
            }
            cmd += " -wordlength 5 -minseqlength 10 -maxrejects 0 -maxaccepts 0 ";
            cmd += " -threads " + params.getInt("cores");
            cmd += " -uc " + resultFile.toString();
            cmd += " -consout " + consensusFile.toString();

            //System.out.println(cmd);
        }

        return cmd;
    }

    @Override
    public void run() {
        DefaultExecutor df = new DefaultExecutor();
        String sep = PlatformUtils.getSeparator();
        
        CommandLine cmdline = new CommandLine(prop.getProperty("UsearchBin")+ sep + "usearch");
        cmdline.addArguments(makeCommand());

        try {
            boolean status = df.execute(cmdline) == 0;
        } catch (IOException ex) {
            Logger.getLogger(Usearch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
