/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.executors;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.immunediversity.util.PlatformUtils;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class UsearchAligner implements Runnable {

    private Properties prop = null;
    private Namespace params = null;
    private File query;
    private File target;
    private File results;
    String cmd = "";
    
     /**
     * @param prop the prop to set
     */
    public void setProp(Properties prop) {
        this.prop = prop;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Namespace params) {
        this.params = params;
    }
    
    /**
     * @param query the query to set
     */
    public void setQuery(File query) {
        this.query = query;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(File target) {
        this.target = target;
    }

    /**
     * @param results the results to set
     */
    public void setResults(File results) {
        this.results = results;
    }
   
    private String makeCommand() {
        String cmd = "";
        
        if (target != null && query != null && results != null && prop != null && params != null) {
            cmd += " -usearch_global ";
            cmd += query.toString();
            cmd += " -db ";
            cmd += target.toString();
            cmd += " -id 0.9  -strand plus -fastapairs ";
            cmd += results.toString();
        }
        
        //System.out.println(cmd);
        return cmd;
    }
    
    @Override
    public void run() {
        DefaultExecutor dfexec = new DefaultExecutor();
        String sep = PlatformUtils.getSeparator();
        
        CommandLine cmdline = new CommandLine(prop.getProperty("UsearchBin")+ sep + "usearch");
        cmdline.addArguments(makeCommand());
        
        try {
            boolean status = dfexec.execute(cmdline) == 0;
        } catch (IOException ex) {
            Logger.getLogger(Usearch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
