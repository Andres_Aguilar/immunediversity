/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.immunediversity.executors;

import java.io.File;
import java.io.IOException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.immunediversity.util.PlatformUtils;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class HMMER {
    private String profil = "";
    private int cores = 1;
    private File inFile;
    private File outFile;
    private File bin;

    /**
     * @return the HMMER profil
     */
    public String getProfil() {
        return profil;
    }

    /**
     * @param profil the HMMER profil to set
     */
    public void setProfil(String profil) {
        this.profil = profil;
    }

    /**
     * @return the cores
     */
    public int getCores() {
        return cores;
    }

    /**
     * @param cores the cores to set
     */
    public void setCores(int cores) {
        this.cores = cores;
    }

    /**
     * @return the inFile
     */
    public File getInFile() {
        return inFile;
    }

    /**
     * @param inFile the inFile to set
     */
    public void setInFile(File inFile) {
        this.inFile = inFile;
    }

    /**
     * @return the outFile
     */
    public File getOutFile() {
        return outFile;
    }

    /**
     * @param outFile the outFile to set
     */
    public void setOutFile(File outFile) {
        this.outFile = outFile;
    }

    /**
     * @return the bin
     */
    public File getBin() {
        return bin;
    }

    /**
     * @param bin the bin to set
     */
    public void setBin(File bin) {
        this.bin = bin;
    }
    
    /**
     * @return execution status
     * @throws java.lang.InterruptedException
     * @throws java.io.IOException
     */
    public boolean runHMMER() throws InterruptedException, IOException {
        boolean status = false;
        if (bin != null && inFile != null && outFile != null) {
            // Files must be configured
            if (bin.exists() && bin.canExecute()) {
                // can execute hmmer binary ?
                if (cores > 0 && cores <= Runtime.getRuntime().availableProcessors()) {
                    String cmd = "--noali";
                    cmd += " --domtblout " + outFile.getAbsolutePath();
                    cmd += " --cpu " + cores;
                    cmd += " " + profil + " ";
                    cmd += inFile.getAbsolutePath();
                    
                    DefaultExecutor df = new DefaultExecutor();
                    CommandLine cmdline = new CommandLine(bin.getAbsolutePath() + PlatformUtils.getSeparator() + "hmmsearch");
                    cmdline.addArguments(cmd);
                    
                    status = df.execute(cmdline) == 0;
                }
            }
        }
        return status;
    }
    
}
