/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.executors;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.biojava3.core.sequence.DNASequence;
import org.immunediversity.IO.Readers;
import org.immunediversity.IO.Writers;
import org.immunediversity.main.MainTests;
import org.immunediversity.util.PlatformUtils;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class IgBLAST {

    private String VdataBase;
    private String DdataBase;
    private String JdataBase;
    private String auxData = "";
    private String domain = "imgt";
    private String strand = "plus";

    private int outfmt;
    private int numThreads;

    private File binPath;

    /* Setters and Getters */
    /**
     * @return the VdataBase
     */
    public String getVdataBase() {
        return VdataBase;
    }

    /**
     * @param VdataBase the VdataBase to set
     */
    public void setVdataBase(String VdataBase) {
        this.VdataBase = VdataBase;
    }

    /**
     * @return the DdataBase
     */
    public String getDdataBase() {
        return DdataBase;
    }

    /**
     * @param DdataBase the DdataBase to set
     */
    public void setDdataBase(String DdataBase) {
        this.DdataBase = DdataBase;
    }

    /**
     * @return the JdataBase
     */
    public String getJdataBase() {
        return JdataBase;
    }

    /**
     * @param JdataBase the JdataBase to set
     */
    public void setJdataBase(String JdataBase) {
        this.JdataBase = JdataBase;
    }

    /**
     * @return the auxData
     */
    public String getAuxData() {
        return auxData;
    }

    /**
     * @param auxData the auxData to set
     */
    public void setAuxData(String auxData) {
        this.auxData = auxData;
    }

    /**
     * @return the binPath
     */
    public File getBinPath() {
        return binPath;
    }

    /**
     * @param binPath the binPath to set
     */
    public void setBinPath(File binPath) {
        this.binPath = binPath;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the outformat code
     */
    public int getOutfmt() {
        return outfmt;
    }

    /**
     * @return the outformat code as String
     */
    public String getStringOutfmt() {
        return Integer.toString(outfmt);
    }

    /**
     * @param outfmt the code of outformat to set
     */
    public void setOutfmt(int outfmt) {
        this.outfmt = outfmt;
    }

    /**
     * @param strand the strand to set
     */
    public void setStrand(String strand) {
        this.strand = strand;
    }

    /**
     * @return the strand
     */
    public String getStrand() {
        return strand;
    }

    /**
     * @param numThreads the number of threads to set
     */
    public void setNumThreads(int numThreads) {
        this.numThreads = numThreads;
    }

    /**
     * @return the number of threads
     */
    public int getNumThreads() {
        return numThreads;
    }

    /**
     * @return the number of threads
     */
    public String getStringNumThreads() {
        return Integer.toString(numThreads);
    }

    /**
     * Method to verify if databases are setted or not
     *
     * @return true if VDJ databases are setted
     */
    public boolean hasDataBases() {
        boolean db = false;

        if (!VdataBase.isEmpty() && !DdataBase.isEmpty() && !JdataBase.isEmpty()) {
            db = true;
        }

        return db;
    }

    /* Private Methods */
    private String makeCommand(File query, File outFile) {
        String command = "";

        // path and executable file 
        String path = getBinPath().getAbsolutePath();
        command += "./igblastn";

        // V Data Base
        command += " -germline_db_V " + getVdataBase();

        // D Data Base
        command += " -germline_db_D " + getDdataBase();

        // J Data Base
        command += " -germline_db_J " + getJdataBase();

        // Domain system 
        command += " -domain_system " + getDomain();

        // Query
        command += " -query " + query.getAbsolutePath();

        // Outfmt
        command += " -outfmt " + getStringOutfmt();

        // Strand
        command += " -strand " + getStrand();

        // Number of threads
        command += " -num_threads " + getStringNumThreads();

        // Out path
        command += " -out " + outFile.getAbsolutePath();

        if (!this.getAuxData().isEmpty()) {
            if (PlatformUtils.isLinux()) {
                command += " -auxilary_data " + getAuxData();
            } else if (PlatformUtils.isMac()) {
                command += " -auxiliary_data " + getAuxData();
            }
        }
        return command;
    }

    /* Public Methods*/
    public boolean runMiltiIgBLAST(File inputFile, File outputFile, int cores, int readsNumber)
            throws IOException {
        boolean status = false;

        if (cores > Runtime.getRuntime().availableProcessors() || cores <= 0) {
            cores = Runtime.getRuntime().availableProcessors();
        }

        if (getBinPath().exists() && inputFile.exists() && inputFile.canRead()) {
            class BlastThread extends Thread {

                private String cmd = "";
                private File binPath;

                public void setcommand(String cmd) {
                    this.cmd = cmd;
                }

                public void setBinPath(File binPath) {
                    this.binPath = binPath;
                }

                @Override
                public void run() {
                    try {

                        Process proc = Runtime.getRuntime().exec(this.cmd, new String[0], this.binPath);

                        proc.waitFor();
                    } catch (IOException | InterruptedException e) {
                    }

                }
            }

            

            Readers read = new Readers();
            Writers write = new Writers();

            List<DNASequence> reads = read.readListDNAFasta(inputFile);

            int size = reads.size();
            int step;

            // calculate step
            step = size / cores;
            System.out.println("\t Size: " + size);
            System.out.println("\t Cores: " + cores);
            System.out.println("\t Step: " + step);

            // simulate data
            int simulate = 0;
            int aux = 0;
            for (int i = 1; i <= cores; i++) {
                if (i == cores) {
                    simulate = size;
                } else {
                    simulate += step;
                }
                System.out.println("\t >> " + aux + " - " + simulate);

                List<DNASequence> sub = reads.subList(aux, simulate);

                try {
                    write.writeNucleotideFasta(sub, new File("/home/andres/pruebas_pipe/3k/res/test/HA7IYCV01_3k_" + i + ".fna"));
                } catch (Exception ex) {
                    Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
                }

                aux = simulate;
            }
            
            BlastThread[] blastThread = new BlastThread[cores];
            
            for (int i = 0; i < cores; i++) {
                blastThread[i].setBinPath(getBinPath());
                String cmd = makeCommand(inputFile, outputFile);
                System.out.println(">>>> command: " + cmd);
                blastThread[i].setcommand(cmd);
            }

        }

        return status;
    }

    /**
     * @param inputFile Fasta input file
     * @param outputFile IgBLAST output file
     * @return Process status
     * @throws java.io.IOException
     * @throws java.lang.InterruptedException
     */
    public boolean runIgBLAST(File inputFile, File outputFile)
            throws IOException, InterruptedException {
        boolean status;

        String cmd = makeCommand(inputFile, outputFile);
        //System.out.println(cmd);
        Process proc = Runtime.getRuntime().exec(cmd, new String[0], getBinPath());

        proc.waitFor();

        // print errors
        InputStream outStream = proc.getErrorStream();
        BufferedInputStream stream = new BufferedInputStream(outStream);
        DataInputStream dis = new DataInputStream(stream);

        while (dis.available() != 0) {
            System.out.println(dis.readLine());
        }

        status = proc.exitValue() == 0;
        return status;
    }

}
