/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.main;

import it.unimi.dsi.fastutil.objects.Reference2ObjectOpenHashMap;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.io.FilenameUtils;
import org.biojava3.core.sequence.DNASequence;
import org.biojava3.core.sequence.ProteinSequence;
import org.immunediversity.IO.Readers;
import org.immunediversity.IO.Writers;
import org.immunediversity.beans.ClonalGroupTemp;
import org.immunediversity.beans.HMMERout;
import org.immunediversity.beans.HeavyChain;
import org.immunediversity.beans.Lineage;
import org.immunediversity.beans.Rearrangement;
import org.immunediversity.executors.HMMER;
import org.immunediversity.executors.IgBLAST;
import org.immunediversity.executors.Usearch;
import org.immunediversity.executors.UsearchAligner;
import org.immunediversity.filters.FastqFilters;
import org.immunediversity.functions.CutSequences;
import org.immunediversity.parsers.HmmerParser;
import org.immunediversity.parsers.IgBlastParser;
import org.immunediversity.parsers.UsearchParser;
import org.immunediversity.util.ObjectSequenceConverter;
import org.immunediversity.util.PlatformUtils;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class HeavyChainWorkFlow {

    private Properties prop = null;
    private Namespace params = null;

    /**
     * @param prop the prop to set
     */
    public void setProp(Properties prop) {
        this.prop = prop;
    }

    /**
     * @param params the params to set
     */
    public void setParams(Namespace params) {
        this.params = params;
    }

    /**
     * Run IgBLAST for heavy chain (human and mouse)
     *
     * @param inFile Input Fastq File
     * @param variant
     * @return LinkedHashMap with all heavy chain information per read
     */
    public LinkedHashMap<String, HeavyChain> runIgBlast(File inFile, String variant) {
        Readers read = new Readers();
        Writers write = new Writers();

        LinkedHashMap<String, HeavyChain> heavyChains = null;

        File outPath = inFile.getParentFile();
        String name = FilenameUtils.removeExtension(inFile.toString());
        String sep = PlatformUtils.getSeparator();

        // Write a fasta file for processing
        File fastaFile = new File(name + ".fna");
        int count = 0;
        try {
            count = write.writeFastq2Fasta(inFile, fastaFile, variant);
        } catch (IOException ex) {
            Logger.getLogger(HeavyChainWorkFlow.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (fastaFile.exists()) {
            IgBLAST blast = new IgBLAST();

            // configure IgBLAST
            String bin = prop.getProperty("blastBinPath");
            blast.setBinPath(new File(bin));

            if (params.getString("species").equalsIgnoreCase("human")) {
                blast.setAuxData(prop.getProperty("blastHumanAuxData"));

                if (params.getString("type").equalsIgnoreCase("hc")) {
                    blast.setVdataBase(prop.getProperty("HumanHeavyChainVDataBase"));
                    blast.setDdataBase(prop.getProperty("HumanHeavyChainDDataBase"));
                    blast.setJdataBase(prop.getProperty("HumanHeavyChainJDataBase"));
                }
            } else if (params.getString("species").equalsIgnoreCase("mouse")) {
                blast.setAuxData(prop.getProperty("blastMouseAuxData"));

                if (params.getString("type").equalsIgnoreCase("hc")) {
                    blast.setVdataBase(prop.getProperty("MouseHeavyChainVDataBase"));
                    blast.setDdataBase(prop.getProperty("MouseHeavyChainDDataBase"));
                    blast.setJdataBase(prop.getProperty("MouseHeavyChainJDataBase"));
                }
            }

            blast.setOutfmt(7);
            blast.setStrand("plus");
            blast.setNumThreads(Runtime.getRuntime().availableProcessors());
            File blastOutputFile = new File(outPath.toString() + sep + "igblast.out");

            if (blast.hasDataBases()) {
                @SuppressWarnings("UnusedAssignment")
                boolean status = true;

                try {
                    System.out.println("  Input file:\t" + fastaFile.toString());
                    System.out.println("  Output file:\t" + blastOutputFile.toString());
                    System.out.println("  Strating process with: " + count + " Sequences!");
                    
                    status = blast.runIgBLAST(fastaFile, blastOutputFile);

                    if (status) {
                        IgBlastParser parser = new IgBlastParser();

                        heavyChains = parser.parseBlastOutput4HeavyChain(blastOutputFile);
                    }

                } catch (IOException | InterruptedException ex) { //IOException
                    System.err.println("ERROR: " + HeavyChainWorkFlow.class.getName() + " runIgBLAST failed!!!");
                    System.err.println(ex.getMessage());
                }
            }
        } else {
            System.err.println("ERROR: file " + fastaFile.toString() + " not found!!!");
        }

        return heavyChains;
    }

    /**
     * Method to cut VDJ segments of Fastq sequences
     *
     * @param heavyChains
     * @param name
     * @param inFile
     * @param variant
     * @return true if file was created false if not
     * @throws java.io.IOException
     */
    public File getOnlyVDJ(LinkedHashMap<String, HeavyChain> heavyChains, String name, File inFile, String variant)
            throws IOException {
        File fastqVDJFile = new File(name + "_VDJ.fastq");
        CutSequences cut = new CutSequences();
        //cut.cutFastqHCSequences(heavyChains, inFile, fastqVDJFile, variant);
        System.out.println("   Done! " + heavyChains.size());

        return fastqVDJFile;
    }

    /**
     * Method to filter sequences by length, quality median and Ns
     *
     * @param fastqVDJFile fastq file with only VDJ sequences
     * @param length minimum length
     * @param quality minimum quality median
     * @return File with all filtered sequences
     */
    public File filters(File fastqVDJFile, int length, int quality) {
        String name = FilenameUtils.removeExtension(fastqVDJFile.toString());

        FastqFilters filters = new FastqFilters();

        File fastqLenFile = new File(name + "_filterByLen.fastq");
        File fastqQualFile = new File(name + "_filterByQual.fastq");
        File fastqFilteredFile = new File(name + "_filtered.fastq");

        int[] res;

        try {
            // filter by length
            res = filters.streamFilterBySize(fastqVDJFile, fastqLenFile, length);

            // filter by quality mean
            if (res[1] > 0) {
                System.out.println("Sequences after filter by length (" + length + "):\t" + res[1]);
                res = filters.streamFilterByQualityMedian(fastqLenFile, fastqQualFile, quality);

                if (res[1] > 0) {
                    // filter Ns
                    System.out.println("Sequences after filter by quality median(" + quality + "):\t" + res[1]);
                    res = filters.streamNFilter(fastqQualFile, fastqFilteredFile);

                    // Remove un used files
                    fastqLenFile.delete();
                    fastqQualFile.delete();

                    if (res[1] > 0) {
                        System.out.println("Sequences after all filters:\t" + res[1]);

                    } else {
                        System.err.println("No more sequences after Ns filter!!!");
                        System.exit(1);
                    }
                } else {
                    System.err.println("No more sequences after quality median!!!");
                    System.exit(1);
                }
            } else {
                System.err.println("No more sequences after length filter!!!");
                System.exit(1);
            }
        } catch (IOException ex) {
            System.err.println("ERROR: " + HeavyChainWorkFlow.class.getName() + " " + ex.getMessage());
        }

        return fastqFilteredFile;
    }

    /**
     *
     * @param inFile
     * @param outPath
     * @param variant
     * @return
     * @throws java.io.IOException
     */
    public Reference2ObjectOpenHashMap<String, HMMERout> runHmmer(File inFile, File outPath, String variant) throws IOException, Exception {
        HMMER hmmr = new HMMER();
        String sep = PlatformUtils.getSeparator();

        String nameTemp = FilenameUtils.removeExtension(inFile.toString()) + ".fna";
        File hmmerFastaFile = new File(nameTemp);

        //read Fastq and write Fasta
        Readers read = new Readers();
        Writers write = new Writers();

        write.writeFastq2Fasta(inFile, hmmerFastaFile, variant);

        File hmmerResultFileV = File.createTempFile(outPath + sep + "hmmerV", "out");
        File hmmerResultFileJ = File.createTempFile(outPath + sep + "hmmerJ", "out");

        //if (params.getString("species").equalsIgnoreCase("human")) {
            hmmr.setProfil(prop.getProperty("HumanHmmerHCVProfile"));
        //} else if (params.getString("species").equalsIgnoreCase("mouse")) {
           // hmmr.setProfil(prop.getProperty("MouseHmmerHCVProfile"));
       // }
        hmmr.setBin(new File(prop.getProperty("HmmerBinPath")));
        hmmr.setInFile(hmmerFastaFile);
        hmmr.setOutFile(hmmerResultFileV);
        hmmr.setCores(Runtime.getRuntime().availableProcessors());

        Reference2ObjectOpenHashMap<String, HMMERout> results = null;

        try {
            hmmr.runHMMER();

            HmmerParser hmmerParser = new HmmerParser();

            // results for V
            Reference2ObjectOpenHashMap<String, HMMERout> resultV;
            resultV = hmmerParser.parseDomtbloutOutput(hmmerResultFileV);

            if (!resultV.isEmpty()) {
                // run HMMER for J
                //if (params.getString("species").equalsIgnoreCase("human")) {
                    hmmr.setProfil(prop.getProperty("HumanHmmerHCJProfile"));
                //} else if (params.getString("species").equalsIgnoreCase("mouse")) {
                    //hmmr.setProfil(prop.getProperty("MouseHmmerHCJProfile"));
                //}
                hmmr.setOutFile(hmmerResultFileJ);
                hmmr.runHMMER();

                // results fot V and J
                results = hmmerParser.parseDomtbloutOutput2(hmmerResultFileJ, resultV);
            }
        } catch (InterruptedException | IOException ex) {
            System.err.println("ERROR: " + ex.getMessage());
        } finally {
            // delete files
            hmmerResultFileJ.delete();
            hmmerResultFileV.delete();
        }
        return results;
    }

    /**
     * @param results
     * @param heavyChains
     * @param filteredSequences
     * @return
     */
    public LinkedHashMap<String, HeavyChain> setCDR3(LinkedHashMap<String, HMMERout> results,
            LinkedHashMap<String, HeavyChain> heavyChains, LinkedHashMap<String, DNASequence> filteredSequences) {

        if (!results.isEmpty()) {
            Set set = results.keySet();
            Iterator<String> it = set.iterator();

            while (it.hasNext()) {
                String id = it.next();
                HMMERout hmmResult = results.get(id);
                HeavyChain hc = heavyChains.get(id);
                DNASequence seq = filteredSequences.get(id);

                if (hc != null && hmmResult != null && seq != null) {
                    int start = hmmResult.getStartAsInt();
                    int end = hmmResult.getEndAsInt() - 1;
                    int len = seq.getLength();

                    hc.setStartCDR3(start);
                    hc.setEndCDR3(end);
                    hc.setSequence(seq);

                    if (start < len && end <= len && start < end) {
                        DNASequence cdr3 = new DNASequence(seq.getSequenceAsString().substring(hc.getStartCDR3(), hc.getEndCDR3()));
                        hc.setCdr3(cdr3);
                    }
                    heavyChains.put(id, hc);
                }
            }
        }
        return heavyChains;
    }
    
    /**
     * @param heavyChains
     * @return LinkedHashMap with only heavy chains with CDR3 sequence
     */
    public LinkedHashMap<String, HeavyChain> removeSequencesWithoutCDR3(LinkedHashMap<String, HeavyChain> heavyChains) {
        LinkedHashMap<String, HeavyChain> results = new LinkedHashMap();

        if (!heavyChains.isEmpty()) {
            Set set = heavyChains.keySet();
            Iterator<String> it = set.iterator();

            while (it.hasNext()) {
                String id = it.next();
                HeavyChain hc = heavyChains.get(id);

                if (hc.hasCDR3Sequence()) {
                    results.put(id, hc);
                }
            }
        }

        return results;
    }

    /**
     * @param heavyChains
     * @return
     */
    public LinkedHashMap<String, Rearrangement> groupByRearrangement(LinkedHashMap<String, HeavyChain> heavyChains) {
        LinkedHashMap<String, Rearrangement> results = new LinkedHashMap();
        if (!heavyChains.isEmpty()) {
            LinkedList<String> rearrangements = new LinkedList();

            // get all rearrangements
            Set set = heavyChains.keySet();
            Iterator<String> it = set.iterator();

            while (it.hasNext()) {
                HeavyChain hc = heavyChains.get(it.next());
                rearrangements.add(hc.getRearrangement());
            }

            // delete duplicates
            Set rearrangementsSet = new HashSet();
            rearrangementsSet.addAll(rearrangements);

            // create collection
            Iterator<String> it2 = rearrangementsSet.iterator();

            while (it2.hasNext()) {
                String rearrangementString = it2.next();

                Set heavyChainsSet = heavyChains.keySet();
                Iterator<String> heavyChainIterator = heavyChainsSet.iterator();

                LinkedList<HeavyChain> rearrangementsTemp = new LinkedList();

                while (heavyChainIterator.hasNext()) {
                    HeavyChain hc = heavyChains.get(heavyChainIterator.next());

                    if (hc.getRearrangement().equalsIgnoreCase(rearrangementString)) {
                        rearrangementsTemp.add(hc);
                    }
                }

                Rearrangement rearrangement = new Rearrangement();
                rearrangement.setRearrangement(rearrangementString);
                rearrangement.setHeavyChains(rearrangementsTemp);

                results.put(rearrangementString, rearrangement);
            }
        }

        return results;
    }

    private ClonalGroupTemp groupClonalSequences(Rearrangement rearrangement, LinkedHashMap<String, Integer> results, int number, LinkedHashMap<String, DNASequence> consensus) {
        ClonalGroupTemp clonalGroup = new ClonalGroupTemp();

        ObjectSequenceConverter conv = new ObjectSequenceConverter();
        LinkedHashMap<String, HeavyChain> heavyChains = conv.List2LinkedHashMap(rearrangement.getHeavyChains());
        LinkedHashMap<String, HeavyChain> hcTempMap = new LinkedHashMap();

        Set resultSet = results.keySet();
        Iterator<String> it = resultSet.iterator();

        while (it.hasNext()) {
            String sequenceID = it.next();
            Integer integer = results.get(sequenceID);

            if (integer == number) {
                HeavyChain hc = heavyChains.get(sequenceID);

                if (hc != null) {
                    hcTempMap.put(sequenceID, hc);
                }
            }
        }

        clonalGroup.setRearrangement(rearrangement.getRearrangement());
        clonalGroup.setHeavyChains(hcTempMap);
        clonalGroup.setNumber(number);

        // set consensus sequence
        Set consensusSet = consensus.keySet();
        Iterator<String> consensusIt = consensusSet.iterator();

        while (consensusIt.hasNext()) {
            String id = consensusIt.next();

            if (clonalGroup.hasID(id)) {
                DNASequence consensusSequence = consensus.get(id);
                consensusSequence.setOriginalHeader(clonalGroup.getName());
                clonalGroup.setConsensusSequence(consensusSequence);
            }
        }

        return clonalGroup;
    }

    private LinkedHashMap<String, ClonalGroupTemp> clonalGroups(Rearrangement rearrangement, LinkedHashMap<String, Integer> results, LinkedHashMap<String, DNASequence> consensus, LinkedHashMap<String, ClonalGroupTemp> clonalGroups) {

        if (rearrangement != null) {
            if (!results.isEmpty() && !consensus.isEmpty()) {
                // get the number of clonal groups
                Set set = results.keySet();
                Iterator<String> it = set.iterator();

                LinkedList<Integer> tempList = new LinkedList();
                while (it.hasNext()) {
                    String string = it.next();
                    tempList.add(results.get(string));
                }

                Set<Integer> resultsSet = new HashSet();
                resultsSet.addAll(tempList);

                Iterator<Integer> itSet = resultsSet.iterator();
                while (itSet.hasNext()) {
                    Integer object = itSet.next();

                    ClonalGroupTemp tempo = groupClonalSequences(rearrangement, results, object, consensus);
                    clonalGroups.put(tempo.getName(), tempo);
                }
            }
        }

        return clonalGroups;
    }

    /**
     * @param rearrangements
     * @return
     * @throws java.io.IOException
     */
    public LinkedHashMap<String, ClonalGroupTemp> doClonalGroups(LinkedHashMap<String, Rearrangement> rearrangements)
            throws IOException, Exception {

        LinkedHashMap<String, ClonalGroupTemp> clonalGroups = new LinkedHashMap();

        if (!rearrangements.isEmpty()) {
            Set set = rearrangements.keySet();
            Iterator<String> it = set.iterator();

            Writers writer = new Writers();

            while (it.hasNext()) {
                String id = it.next();
                Rearrangement rearrangement = rearrangements.get(id);

                File rearrangementFile = File.createTempFile(rearrangement.getRearrangement(), ".fa");
                File consensusFile = File.createTempFile(rearrangement.getRearrangement(), ".cons");
                File resultsFile = File.createTempFile(rearrangement.getRearrangement(), ".uc");

                // write fasta file
                writer.writeRearrangementFile(rearrangement, rearrangementFile);

                Usearch usearch = new Usearch();
                usearch.setParams(params);
                usearch.setProp(prop);
                usearch.setResultFile(resultsFile);
                usearch.setConsensusFile(consensusFile);
                usearch.setFastaFile(rearrangementFile);

                Thread usearchThread = new Thread(usearch);
                usearchThread.start();
                try {
                    usearchThread.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Usearch.class.getName()).log(Level.SEVERE, null, ex);
                }

                UsearchParser parser = new UsearchParser();
                LinkedHashMap<String, Integer> res = new LinkedHashMap<>();
                LinkedHashMap<String, DNASequence> cons = new LinkedHashMap<>();

                try {
                    res = parser.parseResults(resultsFile);
                    cons = parser.readConsensus(consensusFile);

                } catch (IOException ex) {
                    Logger.getLogger(Usearch.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    resultsFile.deleteOnExit();
                    consensusFile.deleteOnExit();
                    rearrangementFile.deleteOnExit();
                }

                clonalGroups = clonalGroups(rearrangement, res, cons, clonalGroups);
            }
        }

        return clonalGroups;
    }

    private Lineage groupLineageSequence(ClonalGroupTemp clonotype, LinkedHashMap<String, Integer> results, int number, LinkedHashMap<String, DNASequence> consensus) {
        Lineage lineage = new Lineage();

        LinkedHashMap<String, HeavyChain> heavyChains = clonotype.getHeavyChains();
        LinkedHashMap<String, HeavyChain> hcTempMap = new LinkedHashMap();

        Set resultSet = results.keySet();
        Iterator<String> it = resultSet.iterator();

        while (it.hasNext()) {
            String sequenceID = it.next();
            Integer integer = results.get(sequenceID);

            if (integer == number) {
                HeavyChain hc = heavyChains.get(sequenceID);

                if (hc != null) {
                    hcTempMap.put(sequenceID, hc);
                }
            }
        }

        lineage.setHeavyChains(hcTempMap);
        lineage.setRearrangement(clonotype.getRearrangement());
        lineage.setClonalGroupNumber(clonotype.getNumber());
        lineage.setLineageNumber(number);

        // set consensus sequence
        Set consensusSet = consensus.keySet();
        Iterator<String> consensusIt = consensusSet.iterator();

        while (consensusIt.hasNext()) {
            String id = consensusIt.next();

            if (lineage.hasID(id)) {
                DNASequence consensusSequence = consensus.get(id);
                consensusSequence.setOriginalHeader(lineage.getName());
                lineage.setConsensusSequence(consensusSequence);
            }
        }

        return lineage;
    }

    private LinkedHashMap<String, Lineage> lineages(ClonalGroupTemp clonotype, LinkedHashMap<String, Integer> results, LinkedHashMap<String, DNASequence> consensus, LinkedHashMap<String, Lineage> lineages) throws IOException {
        if (clonotype != null) {
            if (!results.isEmpty() && !consensus.isEmpty()) {
                // get the number of lineages
                Set set = results.keySet();
                Iterator<String> it = set.iterator();

                LinkedList<Integer> tempList = new LinkedList();
                while (it.hasNext()) {
                    String string = it.next();
                    tempList.add(results.get(string));
                }

                Set<Integer> resultsSet = new HashSet();
                resultsSet.addAll(tempList);

                Iterator<Integer> itSet = resultsSet.iterator();
                while (itSet.hasNext()) {
                    Integer object = itSet.next();

                    Lineage lineageTemp = groupLineageSequence(clonotype, results, object, consensus);
                    lineages.put(lineageTemp.getName(), lineageTemp);
                }
            }
        }
        return lineages;
    }

    /**
     * @param clonalGroups
     * @return
     * @throws java.io.IOException
     */
    public LinkedHashMap<String, Lineage> doLineages(LinkedHashMap<String, ClonalGroupTemp> clonalGroups) throws IOException {
        LinkedHashMap<String, Lineage> lineages = new LinkedHashMap();
        if (!clonalGroups.isEmpty()) {
            Set set = clonalGroups.keySet();
            Iterator<String> it = set.iterator();

            while (it.hasNext()) {
                String id = it.next();
                ClonalGroupTemp clonotype = clonalGroups.get(id);
                LinkedHashMap<String, HeavyChain> heavyChains = clonotype.getHeavyChains();

                if (heavyChains.size() != 0) {
                    File lineagesFile = File.createTempFile(clonotype.getRearrangement(), ".fa");
                    File resultsFile = File.createTempFile(clonotype.getRearrangement(), ".uc");
                    File consensusFile = File.createTempFile(clonotype.getRearrangement(), ".cons");

                    // write sequences
                    Writers write = new Writers();
                    write.writeLineagesFile(heavyChains, lineagesFile);

                    Usearch usearch = new Usearch();
                    usearch.setFastaFile(lineagesFile);
                    usearch.setResultFile(resultsFile);
                    usearch.setConsensusFile(consensusFile);
                    usearch.setParams(params);
                    usearch.setProp(prop);
                    usearch.setID(0.995);
                    usearch.setQfract(0.6);
                    usearch.setTfract(0.6);

                    Thread usearchThread = new Thread(usearch);
                    usearchThread.start();
                    try {
                        usearchThread.join();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Usearch.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    UsearchParser parser = new UsearchParser();
                    LinkedHashMap<String, Integer> res = new LinkedHashMap<>();
                    LinkedHashMap<String, DNASequence> cons = new LinkedHashMap<>();

                    try {
                        res = parser.parseResults(resultsFile);
                        cons = parser.readConsensus(consensusFile);

                    } catch (IOException ex) {
                        System.err.println("ERROR: usearch parser! \n" + ex.getMessage());
                    } finally {
                        lineagesFile.deleteOnExit();
                        resultsFile.deleteOnExit();
                        consensusFile.deleteOnExit();
                    }

                    lineages = lineages(clonotype, res, cons, lineages);
                }
            }
        }

        return lineages;
    }

    /**
     * @param consensusFullVJ file with all consensus sequences
     * @return LinkedHashMap with all heavy chain information per read
     */
    public LinkedHashMap<String, HeavyChain> correctAlleleAssignation(File consensusFullVJ) {
        LinkedHashMap<String, HeavyChain> heavyChains = new LinkedHashMap();

        if (consensusFullVJ.exists()) {
            IgBLAST blast = new IgBLAST();
            String sep = PlatformUtils.getSeparator();

            // configure IgBLAST
            String bin = prop.getProperty("blastBinPath");
            blast.setBinPath(new File(bin));

            if (params.getString("species").equalsIgnoreCase("human")) {
                blast.setAuxData(prop.getProperty("blastHumanAuxData"));

                if (params.getString("type").equalsIgnoreCase("hc")) {
                    blast.setVdataBase(prop.getProperty("HumanHeavyChainVDataBase"));
                    blast.setDdataBase(prop.getProperty("HumanHeavyChainDDataBase"));
                    blast.setJdataBase(prop.getProperty("HumanHeavyChainJDataBase"));
                }
            } else if (params.getString("species").equalsIgnoreCase("mouse")) {
                blast.setAuxData(prop.getProperty("blastMouseAuxData"));

                if (params.getString("type").equalsIgnoreCase("hc")) {
                    blast.setVdataBase(prop.getProperty("MouseHeavyChainVDataBase"));
                    blast.setDdataBase(prop.getProperty("MouseHeavyChainDDataBase"));
                    blast.setJdataBase(prop.getProperty("MouseHeavyChainJDataBase"));
                }
            }

            blast.setOutfmt(7);
            blast.setStrand("plus");
            blast.setNumThreads(Runtime.getRuntime().availableProcessors());
            File blastOutputFile = new File(consensusFullVJ.getParentFile().toString() + sep + "igblast_alleles.out");

            if (blast.hasDataBases()) {
                @SuppressWarnings("UnusedAssignment")
                boolean status = true;

                try {
                    System.out.println("  Input file:\t" + consensusFullVJ.toString());
                    System.out.println("  Output file:\t" + blastOutputFile.toString());
                    status = blast.runIgBLAST(consensusFullVJ, blastOutputFile);

                    if (status) {
                        IgBlastParser parser = new IgBlastParser();

                        heavyChains = parser.parseBlastOutput4ConsensusHeavyChain(blastOutputFile);
                    }

                } catch (Exception ex) { //IOException | InterruptedException
                    System.err.println("ERROR: " + HeavyChainWorkFlow.class.getName() + " runIgBLAST failed!!!");
                    System.err.println(ex.getMessage());
                } finally {
                    //blastOutputFile.deleteOnExit();
                }
            }
        } else {
            System.err.println("ERROR: file " + consensusFullVJ.toString() + " not found!!!");
        }
        return heavyChains;
    }

    public LinkedHashMap<String, HeavyChain> removeConsensusOutOfFrame(LinkedHashMap<String, HeavyChain> heavyChains) {
        LinkedHashMap<String, HeavyChain> inFrame = new LinkedHashMap<>();

        if (!heavyChains.isEmpty()) {
            Set<String> set = heavyChains.keySet();
            Iterator<String> it = set.iterator();

            while (it.hasNext()) {
                HeavyChain hc = heavyChains.get(it.next());

                DNASequence sequence = hc.getSequence();
                ProteinSequence sequenceAA = sequence.getRNASequence().getProteinSequence();

                if (hc.getInFrame()) {
                    if (!sequenceAA.getSequenceAsString().contains("*")) {
                        inFrame.put(hc.getID(), hc);
                    }

                }
            }
        }

        return inFrame;
    }

    /**
     * @param heavyChains
     * @param consensusReads
     * @return
     */
    public LinkedHashMap<String, HeavyChain> setSequences(LinkedHashMap<String, HeavyChain> heavyChains, LinkedHashMap<String, DNASequence> consensusReads) {
        if (!heavyChains.isEmpty() && !consensusReads.isEmpty()) {
            Set set = heavyChains.keySet();
            Iterator<String> it = set.iterator();

            while (it.hasNext()) {
                String id = it.next();

                HeavyChain hc = heavyChains.get(id);
                DNASequence sequence = consensusReads.get(id);

                if (hc != null && sequence != null) {
                    hc.setSequence(sequence);
                    heavyChains.put(id, hc);
                }
            }
        }

        return heavyChains;
    }

    private void alignment(File query, File target, File results) {
        if (query != null && target != null && results != null) {
            UsearchAligner aligner = new UsearchAligner();

            aligner.setParams(params);
            aligner.setProp(prop);
            aligner.setQuery(query);
            aligner.setTarget(target);
            aligner.setResults(results);

            Thread alignerThread = new Thread(aligner);
            alignerThread.start();
            try {
                alignerThread.join();
            } catch (InterruptedException ex) {
                Logger.getLogger(Usearch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public LinkedList<String> kaks(File resultsFile, LinkedList<String> mutations) {
        Readers read = new Readers();
        if (resultsFile.exists() && resultsFile.canRead()) {
            LinkedHashMap<String, DNASequence> aln = new LinkedHashMap<>();
            try {
                aln = read.readDNAFasta(resultsFile);
            } catch (IOException ex) {
                Logger.getLogger(HeavyChainWorkFlow.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (!aln.isEmpty()) {
                if (aln.size() == 2) {
                    Set<String> set = aln.keySet();
                    Iterator<String> it = set.iterator();

                    int cont = 0;
                    DNASequence query = null;
                    DNASequence target = null;

                    while (it.hasNext()) {
                        if (cont == 0) {
                            query = aln.get(it.next());
                            cont++;
                        } else {
                            target = aln.get(it.next());
                        }
                    }

                    ProteinSequence queryProteinSequence = null;
                    ProteinSequence targetProteinSequence = null;

                    if (query != null && target != null) {
                        queryProteinSequence = query.getRNASequence().getProteinSequence();
                        targetProteinSequence = target.getRNASequence().getProteinSequence();
                    }

                    if (queryProteinSequence != null && targetProteinSequence != null) {

                        if (queryProteinSequence.getSequenceAsString().length() == targetProteinSequence.getSequenceAsString().length()) {
                            AtomicInteger sinonimous = new AtomicInteger();
                            AtomicInteger nonsinonimous = new AtomicInteger();

                            String queryString = queryProteinSequence.getSequenceAsString();
                            String targetString = targetProteinSequence.getSequenceAsString();

                            for (int i = 0; i < queryString.length(); i++) {
                                char queryAA = queryString.charAt(i);
                                char targetAA = targetString.charAt(i);

                                int start = i * 3;

                                String codonQuery = query.getSequenceAsString().substring(start, start + 3);
                                String codonTarget = target.getSequenceAsString().substring(start, start + 3);

                                //System.out.println("nuc: " + codonQuery + " - " + codonTarget);
                                //System.out.println("AA:  " + queryAA + " - " + targetAA);
                                // sinonimous mutations
                                if ((codonQuery.charAt(0) != codonTarget.charAt(0)) && queryAA == targetAA) {
                                    sinonimous.incrementAndGet();
                                }
                                if ((codonQuery.charAt(1) != codonTarget.charAt(1)) && queryAA == targetAA) {
                                    sinonimous.incrementAndGet();
                                }
                                if ((codonQuery.charAt(2) != codonTarget.charAt(2)) && queryAA == targetAA) {
                                    sinonimous.incrementAndGet();
                                }

                                // nonsinonimous mutations
                                if (queryAA != '*' && queryAA != 'X') {
                                    if ((codonQuery.charAt(0) != codonTarget.charAt(0)) && queryAA != targetAA) {
                                        nonsinonimous.incrementAndGet();
                                    }
                                    if ((codonQuery.charAt(1) != codonTarget.charAt(1)) && queryAA != targetAA) {
                                        nonsinonimous.incrementAndGet();
                                    }
                                    if ((codonQuery.charAt(2) != codonTarget.charAt(2)) && queryAA != targetAA) {
                                        nonsinonimous.incrementAndGet();
                                    }
                                }

                            }
                            //System.out.println("========================================");
                            //System.out.print(query.getOriginalHeader());
                            //System.out.print("\t" + sinonimous.get());
                            //System.out.println("\t" + nonsinonimous.get());
                            mutations.add(query.getOriginalHeader() + "\t" + sinonimous.get() + "\t" + nonsinonimous.get());
                        }
                    }

                }

            }
        }
        return mutations;
    }

    public LinkedList<String> getMutationsRate(LinkedHashMap<String, HeavyChain> heavyChains) throws IOException {
        LinkedList<String> mutations = new LinkedList();
        
        if (!heavyChains.isEmpty()) {
            LinkedHashMap<String, DNASequence> germlines;
            File germlinesFile = null;
            Readers read = new Readers();

            // read V germlines
            if (params.getString("species").equalsIgnoreCase("human")) {
                germlinesFile = new File(prop.getProperty("HumanHeavyChainVDataBase"));
            } else if (params.getString("species").equalsIgnoreCase("mouse")) {
                germlinesFile = new File(prop.getProperty("MouseHeavyChainVDataBase"));
            }

            germlines = read.readDNAFasta(germlinesFile);

            if (!germlines.isEmpty()) {
                Set set = heavyChains.keySet();
                Iterator<String> it = set.iterator();
                
                while (it.hasNext()) {
                    HeavyChain query = heavyChains.get(it.next());
                    String vgene = query.getVgene() + "*" + query.getAlleleV();
                    DNASequence target = germlines.get(vgene);

                    File queryFile = File.createTempFile("query_" + vgene, ".fa");
                    File targetFile = File.createTempFile("target_" + vgene, ".fa");
                    File resultFile = File.createTempFile("res_" + vgene, ".aln");

                    // write files
                    Writers write = new Writers();

                    // only V segment
                    String seq = query.getSequence().getSequenceAsString();
                    String querySeq = seq.substring(query.getStartV() - 1, query.getEndV());

                    write.writeSequence(query.getID(), querySeq, queryFile);
                    write.writeSequence(target.getOriginalHeader(), target.getSequenceAsString(), targetFile);

                    alignment(queryFile, targetFile, resultFile);
                    if (resultFile.length() != 0) {
                        mutations = kaks(resultFile, mutations);
                    }
                    

                    queryFile.deleteOnExit();
                    targetFile.deleteOnExit();
                    resultFile.deleteOnExit();

                }
            }
        }
        return mutations;
    }
}
