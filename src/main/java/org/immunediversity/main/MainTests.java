/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.biojava3.core.sequence.DNASequence;
import org.biojava3.core.sequence.ProteinSequence;
import org.immunediversity.IO.Readers;
import org.immunediversity.IO.Writers;
import org.immunediversity.beans.BlastCoords;
import org.immunediversity.beans.HMMERout;
import org.immunediversity.beans.HeavyChain;
import org.immunediversity.beans.IgKappa;
import org.immunediversity.beans.IgLamda;
import org.immunediversity.executors.HMMER;
import org.immunediversity.executors.IgBLAST;
import org.immunediversity.filters.FastqFilters;
import org.immunediversity.functions.CutSequences;
import org.immunediversity.parsers.HmmerParser;
import org.immunediversity.parsers.IgBlastParser;
import org.immunediversity.util.PlatformUtils;
import it.unimi.dsi.fastutil.objects.Reference2ObjectOpenHashMap;
import org.immunediversity.beans.HeavyChain2;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class MainTests {

    static final String version = "2.0.0";

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static Map getDifference(Date startDate, Date endDate) {
        java.util.Date gtDate;
        java.util.Date ltDate;
        Map resultadoMap = new HashMap();

        if (startDate.compareTo(endDate) > 0) {
            gtDate = startDate;
            ltDate = endDate;
        } else {
            gtDate = endDate;
            ltDate = startDate;
        }

        long diffMils = gtDate.getTime() - ltDate.getTime();
        long seconds = diffMils / 1000;
        long hours = seconds / 3600;
        seconds -= hours * 3600;

        long minutes = seconds / 60;
        seconds -= minutes * 60;

        resultadoMap.put("Hrs", Long.toString(hours));
        resultadoMap.put("Min", Long.toString(minutes));
        resultadoMap.put("Sec", Long.toString(seconds));
        return resultadoMap;
    }

    private static void testHCAll(File inFile, String variant, Properties prop) {
        Readers read = new Readers();
        Writers write = new Writers();

        File outPath = inFile.getParentFile();
        String[] nameWext = inFile.getName().split("\\.");
        String name = nameWext[0];
        String sep = PlatformUtils.getSeparator();

        // read FASTQ to write a fasta file to run IgBLAST
        LinkedHashMap<String, DNASequence> allSequences = new LinkedHashMap();
        try {
            allSequences = read.readFastq2LinkedHashMap(inFile, variant);
        } catch (IOException ex) {
            Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!allSequences.isEmpty() && outPath.canWrite()) {
            File fullFastaFile = new File(outPath.toString() + sep + name + ".fna");

            try {
                write.writeNucleotideFasta(allSequences, fullFastaFile);
            } catch (Exception ex) {
                Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
            }

            // configure IgBLAST
            IgBLAST blast = new IgBLAST();
            String bin = prop.getProperty("blastBinPath");

            // configure IgBLAST
            blast.setBinPath(new File(bin));
            blast.setVdataBase(prop.getProperty("HumanHeavyChainVDataBase"));
            blast.setDdataBase(prop.getProperty("HumanHeavyChainDDataBase"));
            blast.setJdataBase(prop.getProperty("HumanHeavyChainJDataBase"));
            blast.setAuxData(prop.getProperty("blastHumanAuxData"));
            blast.setOutfmt(7);
            blast.setStrand("plus");
            blast.setNumThreads(Runtime.getRuntime().availableProcessors());
            File blastOutputFile = new File(outPath.toString() + sep + "igblast.out");

            @SuppressWarnings("UnusedAssignment")
            boolean status = true;

            try {
                System.out.println("Running IgBLAST");
                status = blast.runIgBLAST(fullFastaFile, blastOutputFile);

                if (status) {
                    IgBlastParser parser = new IgBlastParser();
                    LinkedHashMap<String, HeavyChain> heavyChains = parser.parseBlastOutput4HeavyChain(blastOutputFile);

                    // Delete files
                    fullFastaFile.delete();
                    blastOutputFile.delete();

                    if (!heavyChains.isEmpty()) {
                        System.out.println("Cut only VDJ segments");
                        File fastqVDJFile = new File(outPath.toString() + sep + name + "_VDJ.fastq");
                        CutSequences cut = new CutSequences();
                        //cut.cutFastqHCSequences(heavyChains, inFile, fastqVDJFile, variant);

                        /* Filter FASTQ sequences */
                        System.out.println("Filtering sequences");
                        FastqFilters filters = new FastqFilters();
                        int[] res;

                        // filter by length
                        File fastqLenFile = new File(outPath.toString() + sep + name + "_filterByLen.fastq");
                        res = filters.streamFilterBySize(fastqVDJFile, fastqLenFile, 200);

                        // filter by quality mean
                        if (res[1] > 0) {
                            System.out.println("After filter by length " + res[1]);
                            File fastqQualFile = new File(outPath.toString() + sep + name + "_filterByQual.fastq");
                            res = filters.streamFilterByQualityMedian(fastqLenFile, fastqQualFile, 28);

                            if (res[1] > 0) {
                                // filter Ns
                                System.out.println("After filter by quality median: " + res[1]);
                                File fastqFilteredFile = new File(outPath.toString() + sep + name + "_filtered.fastq");
                                res = filters.streamNFilter(fastqQualFile, fastqFilteredFile);

                                // Remove un used files
                                fastqLenFile.delete();
                                fastqQualFile.delete();

                                if (res[1] > 0) {
                                    System.out.println("After all filters " + res[1]);

                                    // Writting fasta file to run HMMER
                                    File fastaFilteredFile = new File(outPath.toString() + sep + name + "_filtered.fna");
                                    LinkedHashMap<String, DNASequence> filteredSequences = read.readFastq2LinkedHashMap(fastqFilteredFile, variant);
                                    if (!filteredSequences.isEmpty()) {
                                        try {
                                            write.writeNucleotideFasta(filteredSequences, fastaFilteredFile);
                                        } catch (Exception ex) {
                                            Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
                                        }

                                        HMMER hmmr = new HMMER();

                                        File hmmerResultFileV = new File(outPath + sep + "hmmerV.out");
                                        File hmmerResultFileJ = new File(outPath + sep + "hmmerJ.out");

                                        hmmr.setProfil(prop.getProperty("HumanHmmerHCVProfile"));
                                        hmmr.setBin(new File(prop.getProperty("HmmerBinPath")));
                                        hmmr.setInFile(fastaFilteredFile);
                                        hmmr.setOutFile(hmmerResultFileV);
                                        hmmr.setCores(Runtime.getRuntime().availableProcessors());
                                        try {
                                            hmmr.runHMMER();

                                            HmmerParser hmmerParser = new HmmerParser();

                                            // results for V
                                            Reference2ObjectOpenHashMap<String, HMMERout> resultV;
                                            resultV = hmmerParser.parseDomtbloutOutput(hmmerResultFileV);

                                            if (!resultV.isEmpty()) {
                                                // run HMMER for J
                                                hmmr.setProfil(prop.getProperty("HumanHmmerHCJProfile"));
                                                hmmr.setOutFile(hmmerResultFileJ);
                                                hmmr.runHMMER();

                                                // results fot J
                                                Reference2ObjectOpenHashMap<String, HMMERout> results;
                                                results = hmmerParser.parseDomtbloutOutput2(hmmerResultFileJ, resultV);

                                                // delete files
                                                hmmerResultFileJ.delete();
                                                hmmerResultFileV.delete();
                                                fastaFilteredFile.delete();

                                                if (!results.isEmpty()) {
                                                    Set set = results.keySet();
                                                    Iterator<String> it = set.iterator();

                                                    while (it.hasNext()) {
                                                        String id = it.next();
                                                        HMMERout hmmResult = results.get(id);
                                                        HeavyChain hc = heavyChains.get(id);
                                                        DNASequence seq = filteredSequences.get(id);

                                                        if (hc != null && hmmResult != null && seq != null) {
                                                            int start = hmmResult.getStartAsInt();
                                                            int end = hmmResult.getEndAsInt() - 1;
                                                            int len = seq.getLength();

                                                            hc.setStartCDR3(start);
                                                            hc.setEndCDR3(end);
                                                            hc.setSequence(seq);

                                                            if (start < len && end <= len && start < end) {
                                                                DNASequence cdr3 = new DNASequence(seq.getSequenceAsString().substring(hc.getStartCDR3(), hc.getEndCDR3()));
                                                                hc.setCdr3(cdr3);
                                                            }
                                                            //hc.print();

                                                            heavyChains.put(id, hc);
                                                        }
                                                    }

                                                    File vdjFile = new File(outPath.toString() + sep + "VDJs.txt");
                                                    File CDR3file = new File(outPath.toString() + sep + "CDR3.fna");
                                                    write.writeVDJTSVFile(heavyChains, vdjFile);
                                                    write.writeCDR3file(heavyChains, CDR3file);
                                                }
                                            }

                                        } catch (InterruptedException | IOException ex) { //InterruptedException |
                                            System.err.println("ERROR: " + ex.getMessage());
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

            } catch (IOException | InterruptedException ex) {
                Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private static void testBlastKappa(File inFile, File outFile) {
        IgBLAST blast = new IgBLAST();
        String bin = "/opt/ImmunediveRsity/data/IGBLAST/";
        String home = "/home/andres/Documentos/Dropbox/Documentos/INSP/Scripts/DataBases/";

        // configure IgBLAST
        blast.setBinPath(new File(bin));
        blast.setVdataBase(home + "human/LC/kappa/IgKV");
        blast.setDdataBase(home + "human/HC/IgHD");
        blast.setJdataBase(home + "human/LC/kappa/IgKJ");
        blast.setAuxData(home + "optional_file/human_gl.aux");
        blast.setOutfmt(7);
        blast.setStrand("plus");
        blast.setNumThreads(Runtime.getRuntime().availableProcessors());

        @SuppressWarnings("UnusedAssignment")
        boolean status = true;

        try {
            status = blast.runIgBLAST(inFile, outFile);

            if (status) {
                IgBlastParser kappaParser = new IgBlastParser();

                LinkedHashMap<String, IgKappa> kappas = kappaParser.parseBlastOutput4Kappa(outFile);

                if (!kappas.isEmpty()) {
                    System.out.println("Elements: " + kappas.size());

                    File fastqFile = new File("/home/andres/pruebas_pipe/recorteLigeras/kappa/kappa_trimm.fastq");
                    File fastqVDJFile = new File("/home/andres/pruebas_pipe/recorteLigeras/kappa/kappa_trimm_VJ.fastq");
                    CutSequences cut = new CutSequences();

                    cut.cutFastqKCSequences(kappas, fastqFile, fastqVDJFile, "sanger");

                    /* Filter FASTQ sequences */
                    FastqFilters filters = new FastqFilters();
                    int[] res;

                    // filter by length
                    File fastqLenFile = new File("/home/andres/pruebas_pipe/recorteLigeras/kappa/kappa_trimm_VJ_filterByLen.fastq");
                    res = filters.streamFilterBySize(fastqVDJFile, fastqLenFile, 200);

                    // filter by quality mean
                    if (res[1] > 0) {
                        System.out.println("Filtradas por longitud de 200: " + res[1]);
                        File fastqQualFile = new File("/home/andres/pruebas_pipe/recorteLigeras/kappa/kappa_trimm_VJ_filterByQual.fastq");
                        res = filters.streamFilterByQualityMedian(fastqLenFile, fastqQualFile, 28);

                        if (res[1] > 0) {
                            // filter Ns
                            System.out.println("Filtradas por longitud de media de calidad a 28: " + res[1]);
                            File fastqFilteredFile = new File("/home/andres/pruebas_pipe/recorteLigeras/kappa/kappa_trimm_VJ_filtered.fastq");
                            res = filters.streamNFilter(fastqQualFile, fastqFilteredFile);

                            // Remove un used files
                            fastqLenFile.delete();
                            fastqQualFile.delete();

                            if (res[1] > 0) {
                                System.out.println("Todo bien!");
                                System.out.println("Quedaron " + res[1] + " secuencias");
                            }
                        }
                    }
                }
            }
        } catch (IOException | InterruptedException ex) { //
            Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void testBlastLamda(File inFile, File outFile) {
        IgBLAST blast = new IgBLAST();
        String bin = "/opt/ImmunediveRsity/data/IGBLAST/";
        String home = "/home/andres/Documentos/Dropbox/Documentos/INSP/Scripts/DataBases/";

        // configure IgBLAST
        blast.setBinPath(new File(bin));
        blast.setVdataBase(home + "human/LC/lamda/IgLV");
        blast.setDdataBase(home + "human/HC/IgHD");
        blast.setJdataBase(home + "human/LC/lamda/IgLJ");
        blast.setAuxData(home + "optional_file/human_gl.aux");
        blast.setOutfmt(7);
        blast.setStrand("plus");
        blast.setNumThreads(Runtime.getRuntime().availableProcessors());

        @SuppressWarnings("UnusedAssignment")
        boolean status = true;

        try {
            status = blast.runIgBLAST(inFile, outFile);

            if (status) {
                IgBlastParser parser = new IgBlastParser();
                LinkedHashMap<String, IgLamda> lamdas = parser.parseBlastOutput4Lamda(outFile);

                if (!lamdas.isEmpty()) {
                    System.out.println("Elementos: " + lamdas.size());

                    File fastqFile = new File("/home/andres/pruebas_pipe/recorteLigeras/lamda/lamda.fastq");
                    File fastqVDJFile = new File("/home/andres/pruebas_pipe/recorteLigeras/lamda/lamda_VDJ.fastq");

                    CutSequences cut = new CutSequences();
                    cut.cutFastqLCSequences(lamdas, fastqFile, fastqVDJFile, "sanger");

                    /* Filter FASTQ sequences */
                    FastqFilters filters = new FastqFilters();
                    int[] res;

                    // filter by length
                    File fastqLenFile = new File("/home/andres/pruebas_pipe/recorteLigeras/lamda/lamda_filterByLen.fastq");
                    res = filters.streamFilterBySize(fastqVDJFile, fastqLenFile, 200);

                    // filter by quality mean
                    if (res[1] > 0) {
                        System.out.println("Filtradas por longitud de 200: " + res[1]);
                        File fastqQualFile = new File("/home/andres/pruebas_pipe/recorteLigeras/lamda/lamda_filterByQual.fastq");
                        res = filters.streamFilterByQualityMedian(fastqLenFile, fastqQualFile, 28);

                        if (res[1] > 0) {
                            // filter Ns
                            System.out.println("Filtradas por longitud de media de calidad a 28: " + res[1]);
                            File fastqFilteredFile = new File("/home/andres/pruebas_pipe/recorteLigeras/lamda/lamda_filtered.fastq");
                            res = filters.streamNFilter(fastqQualFile, fastqFilteredFile);

                            // Remove un used files
                            fastqLenFile.delete();
                            fastqQualFile.delete();

                            if (res[1] > 0) {
                                System.out.println("Todo bien!");
                                System.out.println("Quedaron " + res[1] + " secuencias");
                            }
                        }
                    }
                }
            }
        } catch (IOException | InterruptedException ex) { //
            Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Properties readProperties(File propFile) {
        Properties prop = new Properties();
        InputStream input = null;

        if (propFile.exists() && propFile.canRead()) {
            try {
                input = new FileInputStream(propFile);

                // load properties file
                prop.load(input);

                // get the property value and print
                System.out.print("Loading configuration file: ");
                if (!prop.getProperty("blastBinPath").isEmpty() && !prop.getProperty("HmmerBinPath").isEmpty()) {
                    System.out.println("\tdone!");
                } else {
                    System.err.println("ERROR: configuration file not correct!!!");
                    System.exit(1);
                }

            } catch (IOException ex) {
                System.err.println("ERROR: " + ex.getMessage());
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        System.err.println("ERROR: " + e.getMessage());
                    }
                }
            }
        }
        return prop;
    }

    private static Namespace testParams(String[] args) {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("Immunediversity")
                .defaultHelp(true)
                .description("Manipulation and processing of HTS reads to identify VDJ "
                        + "usage and clonal origin to gain insight of the antibody "
                        + "repertoire of a given organism.")
                .epilog("Immunediversity is GNU software")
                .version("${prog} " + version);

        parser.addArgument("-v", "--version").action(Arguments.version());

        parser.addArgument("-i", "--input")
                .help("Full path to your fastq file.")
                .required(true);

        parser.addArgument("-o", "-output")
                .help("Full output path.")
                .required(true);

        parser.addArgument("-c", "--cores")
                .setDefault(1)
                .type(Integer.class)
                .help("Number of cores to use.");

        parser.addArgument("-l", "--length")
                .setDefault(200)
                .type(Integer.class)
                .help("Minimum read length to consider.");

        parser.addArgument("-qm", "--quality_median")
                .setDefault(28)
                .type(Integer.class)
                .help("Quality median, Filter by quality median.");

        parser.addArgument("-s", "--species")
                .choices("human", "mouse")
                .setDefault("human")
                .help("Species to work, can be human or mouse.");

        @SuppressWarnings("UnusedAssignment")
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        return ns;
    }

    private static void splitFile() {
        Readers read = new Readers();
        Writers write = new Writers();

        int threads = 7;
        int step;

        try {
            List<DNASequence> reads = read.readListDNAFasta(new File("/home/andres/pruebas_pipe/3k/res/test/HA7IYCV01_3k.fna"));

            int size = reads.size();

            // calculate step
            step = size / threads;
            System.out.println("Size: " + size);
            System.out.println("Threads: " + threads);
            System.out.println("step: " + step);

            // simulate data
            int simulate = 0;
            int aux = 0;
            for (int i = 1; i <= threads; i++) {
                if (i == threads) {
                    simulate = size;
                } else {
                    simulate += step;
                }
                System.out.println(aux + " - " + simulate);

                List<DNASequence> sub = reads.subList(aux, simulate);

                try {
                    write.writeNucleotideFasta(sub, new File("/home/andres/pruebas_pipe/3k/res/test/HA7IYCV01_3k_" + i + ".fna"));
                } catch (Exception ex) {
                    Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
                }

                aux = simulate;
            }
        } catch (IOException ex) {
            Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void kaks(DNASequence query, DNASequence target) {
        ProteinSequence queryProteinSequence = null;
        ProteinSequence targetProteinSequence = null;
        LinkedList<String> results = new LinkedList();

        if (query != null && target != null) {
            queryProteinSequence = query.getRNASequence().getProteinSequence();
            targetProteinSequence = target.getRNASequence().getProteinSequence();
        }

        if (queryProteinSequence != null && targetProteinSequence != null) {

            if (queryProteinSequence.getSequenceAsString().length() == targetProteinSequence.getSequenceAsString().length()) {
                AtomicInteger sinonimous = new AtomicInteger();
                AtomicInteger nonsinonimous = new AtomicInteger();

                String queryString = queryProteinSequence.getSequenceAsString();
                String targetString = targetProteinSequence.getSequenceAsString();

                for (int i = 0; i < queryString.length(); i++) {
                    boolean incluir = false;
                    char queryAA = queryString.charAt(i);
                    char targetAA = targetString.charAt(i);

                    int start = i * 3;

                    String codonQuery = query.getSequenceAsString().substring(start, start + 3);
                    String codonTarget = target.getSequenceAsString().substring(start, start + 3);

                    //System.out.println((start+1) + " -> nuc: " + codonQuery + " - " + codonTarget);
                    //System.out.println((i+1) + " -> AA:  " + queryAA + " - " + targetAA);
                    // sinonimous mutations
                    int cambio = start;
                    int cambio2 = 0;
                    char codonQuery1 = ' ', codonTarget1 = ' ';

                    if ((codonQuery.charAt(0) != codonTarget.charAt(0)) && queryAA == targetAA) {
                        sinonimous.incrementAndGet();
                        cambio += 1;
                        cambio2 = 1;
                        codonQuery1 = codonQuery.charAt(0);
                        codonTarget1 = codonTarget.charAt(0);
                        incluir = true;
                    }
                    if ((codonQuery.charAt(1) != codonTarget.charAt(1)) && queryAA == targetAA) {
                        sinonimous.incrementAndGet();
                        cambio += 2;
                        cambio2 = 2;
                        codonQuery1 = codonQuery.charAt(1);
                        codonTarget1 = codonTarget.charAt(1);
                        incluir = true;
                    }
                    if ((codonQuery.charAt(2) != codonTarget.charAt(2)) && queryAA == targetAA) {
                        sinonimous.incrementAndGet();
                        codonQuery1 = codonQuery.charAt(2);
                        codonTarget1 = codonTarget.charAt(2);
                        cambio += 3;
                        cambio2 = 3;
                        incluir = true;
                    }

                    // nonsinonimous mutations
                    if (queryAA != '*' && queryAA != 'X') {
                        if ((codonQuery.charAt(0) != codonTarget.charAt(0)) && queryAA != targetAA) {
                            nonsinonimous.incrementAndGet();
                            cambio += 1;
                            cambio2 = 1;
                            codonQuery1 = codonQuery.charAt(0);
                            codonTarget1 = codonTarget.charAt(0);
                            incluir = true;
                        }
                        if ((codonQuery.charAt(1) != codonTarget.charAt(1)) && queryAA != targetAA) {
                            nonsinonimous.incrementAndGet();
                            cambio += 2;
                            cambio2 = 2;
                            codonQuery1 = codonQuery.charAt(1);
                            codonTarget1 = codonTarget.charAt(1);
                            incluir = true;
                        }
                        if ((codonQuery.charAt(2) != codonTarget.charAt(2)) && queryAA != targetAA) {
                            nonsinonimous.incrementAndGet();
                            cambio += 3;
                            cambio2 = 3;
                            codonQuery1 = codonQuery.charAt(2);
                            codonTarget1 = codonTarget.charAt(2);
                            incluir = true;
                        }
                    }

                    StringBuilder res = new StringBuilder();
                    res.append(query.getOriginalHeader() + "\t");
                    res.append((cambio) + "\t" + (cambio) + "\t");
                    res.append(codonTarget1 + "\t" + codonQuery1 + "\t");
                    res.append(codonQuery + "\t" + codonTarget + "\t");
                    res.append(queryAA + "\t" + targetAA + "\t");
                    res.append((i + 1) + "\t");
                    if (queryAA == targetAA) {
                        res.append("KS\t");
                    } else {
                        res.append("KA\t");
                    }
                    res.append(cambio2);
                    if (incluir) {
                        results.add(res.toString());
                    }
                }

                //System.out.println("========================================");
                //System.out.print(query.getOriginalHeader());
//                Iterator<String> it = results.iterator();
//                while (it.hasNext()) {
//                    System.out.println(it.next());
//                }
                //System.out.print("\t" + sinonimous.get());
                //System.out.println("\t" + nonsinonimous.get());
            }
        }
    }

    private static void kaksTest() {
        Readers read = new Readers();
        File aisladosFile = new File("/home/andres/Descargas/H7N3.mfa");
        File referenciaFile = new File("/home/andres/Descargas/refH7N3.fa");

        LinkedHashMap<String, DNASequence> aislados = null;
        LinkedHashMap<String, DNASequence> referencia = null;
        try {
            aislados = read.readDNAFasta(aisladosFile);
            referencia = read.readDNAFasta(referenciaFile);
        } catch (IOException ex) {
            Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!aislados.isEmpty() && !referencia.isEmpty()) {
            DNASequence ref = referencia.get("JX397993/1-1723");

            Set set = aislados.keySet();
            Iterator<String> it = set.iterator();

            while (it.hasNext()) {
                String id = it.next();
                DNASequence query = aislados.get(id);

                kaks(query, ref);
            }
        }

    }

    private static void TestReadFastqL() {
        Readers read = new Readers();
        File fastq = new File("/home/andres/pruebas_pipe/500K/miseq500k_s.fastq");
        File fasta = new File("/home/andres/pruebas_pipe/500K/miseq500k_s.fna");

        Writers write = new Writers();
        try {
            write.writeFastq2Fasta(fastq, fasta, "sanger");
            System.out.println("DONE!!!");
        } catch (IOException ex) {
            Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void testParserBlastCoords(Properties prop) {
        IgBlastParser parser = new IgBlastParser();

        //File blastout = new File("/home/andres/pruebas_pipe/3k/res/HA7IYCV01_3k/igblast.out");
        File blastout = new File("/home/andres/pruebas_pipe/500K/res/igblast.out");
        Reference2ObjectOpenHashMap<String, BlastCoords> res = parser.parseBlastOutGetCoords(blastout);

        if (!res.isEmpty()) {
            System.out.println("Cut only VDJ segments");
            
            for (Map.Entry<String, BlastCoords> entry : res.entrySet()) {
                String string = entry.getKey();
                System.out.println(string);
            }
            try {
                System.out.println(res.get("M02081_45_000000000-A8DCH_1_1109_22410_11285").toString());
            } catch (Exception e) {System.out.println("No");}

//            File inFile = new File("/home/andres/pruebas_pipe/500K/res/miseq500k.fastq");
//            File fastqVDJFile = new File("/home/andres/pruebas_pipe/500K/res/miseq500k_test.fastq");
//            File cdr3File = new File("/home/andres/pruebas_pipe/500K/res/CDR3_test.fa");
//            File vdjsFile = new File("/home/andres/pruebas_pipe/500K/res/VDJs_test.tsv");
//
//            CutSequences cut = new CutSequences();
//            try {
//                cut.cutFastqHCSequences(res, inFile, fastqVDJFile, "sanger");
//                res.clear();  // clean memmory (delete all objects in collection)
//            } catch (IOException ex) {
//                Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            HeavyChainWorkFlow hcw = new HeavyChainWorkFlow();
//            hcw.setProp(prop);
//            
//            File filteredFastqFile = hcw.filters(fastqVDJFile, 200, 28);
//            
//            System.out.println("\n >HMMER process");
//            Reference2ObjectOpenHashMap<String, HMMERout> hmmerResults = null;
//            try {
//                hmmerResults = hcw.runHmmer(filteredFastqFile, filteredFastqFile.getParentFile(), "sanger");
//            } catch (Exception ex) {
//                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//            Writers writer = new Writers();
//            //Readers reader = new Readers();
//            try {
//                writer.writeCDR3FileStream(filteredFastqFile, cdr3File, hmmerResults, "sanger");
//            } catch (IOException ex) {
//                Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            
//            IgBlastParser igparser = new IgBlastParser();
//            Reference2ObjectOpenHashMap<String, HeavyChain2> heavyChainList = igparser.parseBlastOutHeavyChain(blastout);
//            
//            try {
//                writer.writeVDJsFile(heavyChainList, vdjsFile, hmmerResults);
//            } catch (IOException ex) {
//                Logger.getLogger(MainTests.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Immunediversity Tests!!!");

        Date start = new Date();
        //testParams(args);
        Properties prop = readProperties(new File("/home/andres/pruebas_pipe/config.prop"));
        //System.out.println("MouseHeavyChainVDataBase: " + prop.getProperty("MouseHeavyChainVDataBase").isEmpty());

        /* Test for Heavy chains */
        //testHCAll(new File("/home/andres/pruebas_pipe/3k/HA7IYCV01_3k.fastq"), "sanger", prop);

        /* Test for light chain (Kappa) */
        //File blastInFileKappa = new File("/home/andres/pruebas_pipe/recorteLigeras/kappa/kappa_trimm.fna");
        //File blastOutFileKappa = new File("/home/andres/pruebas_pipe/recorteLigeras/kappa/igblast.out");
        //testBlastKappa(blastInFileKappa, blastOutFileKappa);

        /* Test for light chain (Lamda) */
        //File blastInFileLamda = new File("/home/andres/pruebas_pipe/recorteLigeras/lamda/lamda.fna");
        //File blastOutFileLamda = new File("/home/andres/pruebas_pipe/recorteLigeras/lamda/igblast.out");
        //testBlastLamda(blastInFileLamda, blastOutFileLamda);
        //String name = FilenameUtils.getBaseName(blastInFileKappa.toString());
        //System.out.println("Nombre: " + name);
        //System.out.println("" + FilenameUtils.removeExtension(blastInFileKappa.toString()));
        /* Split Fasta */
        //splitFile();
        //kaksTest();
        //TestReadFastqL();
        testParserBlastCoords(prop);

        Date end = new Date();
        Map diff = getDifference(start, end);
        System.out.println("\nFinished in:\t" + diff.get("Hrs") + "Hrs " + diff.get("Min") + "Min " + diff.get("Sec") + "Secs");
    }
}
