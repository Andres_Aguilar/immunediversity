/*
 * Copyright (C) 2015 Andres Aguilar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.immunediversity.main;

import it.unimi.dsi.fastutil.objects.Reference2ObjectOpenHashMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.biojava3.core.sequence.DNASequence;
import org.immunediversity.IO.Readers;
import org.immunediversity.IO.Writers;
import org.immunediversity.beans.ClonalGroupTemp;
import org.immunediversity.beans.HMMERout;
import org.immunediversity.beans.HeavyChain;
import org.immunediversity.beans.Lineage;
import org.immunediversity.beans.Rearrangement;
import org.immunediversity.executors.Usearch;
import org.immunediversity.util.PlatformUtils;

/**
 *
 * @author Andres Aguilar <andresyoshimar@gmail.com>
 */
public class Main {

    static final String version = "2.0.0";
    static final String description = "Manipulation and processing of HTS reads to "
            + "identify VDJ usage and clonal origin to gain insight of "
            + "the antibody repertoire of a given organism.";

    @SuppressWarnings({"rawtypes", "unchecked"})
    private static Map getDifference(Date startDate, Date endDate) {
        java.util.Date gtDate;
        java.util.Date ltDate;
        Map resultadoMap = new HashMap();

        if (startDate.compareTo(endDate) > 0) {
            gtDate = startDate;
            ltDate = endDate;
        } else {
            gtDate = endDate;
            ltDate = startDate;
        }

        long diffMils = gtDate.getTime() - ltDate.getTime();
        long seconds = diffMils / 1000;
        long hours = seconds / 3600;
        seconds -= hours * 3600;

        long minutes = seconds / 60;
        seconds -= minutes * 60;

        resultadoMap.put("Hrs", Long.toString(hours));
        resultadoMap.put("Min", Long.toString(minutes));
        resultadoMap.put("Sec", Long.toString(seconds));
        return resultadoMap;
    }

    /**
     * @param args Command line arguments
     * @return Namespace with all parsed arguments
     */
    private static Namespace parseParams(String[] args) {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("Immunediversity")
                .defaultHelp(true)
                .description(description)
                .epilog("Immunediversity is GNU software")
                .version("${prog} " + version);

        parser.addArgument("-v", "--version")
                .action(Arguments.version())
                .help("Show Immunediversity version");

        parser.addArgument("--window")
                .action(Arguments.storeTrue())
                .help("Show window user interface");

        parser.addArgument("--conf")
                .help("Full path to configuration file");

        parser.addArgument("-i", "--input")
                .help("Full path to your fastq file.");

        parser.addArgument("-o", "--output")
                .help("Full output path.");

        parser.addArgument("-c", "--cores")
                .setDefault(1)
                .type(Integer.class)
                .help("Number of cores to use.");

        parser.addArgument("-l", "--length")
                .setDefault(200)
                .type(Integer.class)
                .help("Minimum read length to consider.");

        parser.addArgument("-qm", "--quality_median")
                .setDefault(28)
                .type(Integer.class)
                .help("Quality median, Filter by quality median.");

        parser.addArgument("-s", "--species")
                .choices("human", "mouse")
                .setDefault("human")
                .help("Species to work, can be human or mouse.");

        parser.addArgument("-t", "--type")
                .choices("hc", "kc", "lc")
                .setDefault("hc")
                .help("Type od chain to work. Heavy chain (hc), Kappa chain(kc) or Lamda chain (lc)");

        parser.addArgument("-d", "--id")
                .setDefault(0.97)
                .type(Double.class)
                .help("Minimum identity. Used by usearch.");

        @SuppressWarnings("UnusedAssignment")
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            //System.exit(1);
        }
        return ns;
    }

    /**
     * @param properties file
     * @return Properties object
     */
    private static Properties readProperties(File propFile) {
        Properties prop = new Properties();
        InputStream input = null;

        if (propFile.exists() && propFile.canRead()) {
            try {
                input = new FileInputStream(propFile);

                // load properties file
                prop.load(input);

                // Testing
                System.out.print("Loading configuration file: ");
                if (!prop.getProperty("blastBinPath").isEmpty() && !prop.getProperty("HmmerBinPath").isEmpty()) {
                    System.out.println("\tdone!");
                } else {
                    System.err.println("ERROR: configuration file not correct!!!");
                    System.exit(1);
                }

            } catch (IOException ex) {
                System.err.println("ERROR: " + ex.getMessage());
            } finally {
                if (input != null) {
                    try {
                        input.close();
                    } catch (IOException e) {
                        System.err.println("ERROR: " + e.getMessage());
                        System.exit(1);
                    }
                }
            }
        }
        return prop;
    }

    /**
     * @param Namespace with all parsed params
     * @param Properties configuration properties
     * @return boolean value (true if all is correct)
     */
    private static boolean testConfiguration(Namespace params, Properties prop) {
        boolean blast = false, hmmer = false, usearch = false;

        boolean humanHCDataBases = false, humanKCDataBases = false, humanLCDataBases = false;
        boolean humanHCProfils = false, humanKCProfils = false, humanLCProfils = false;

        boolean mouseHCDataBases = false, mouseKCDataBases = false, mouseLCDataBases = false;
        boolean mouseHCProfils = false, mouseKCProfils = false, mouseLCProfils = false;

        boolean canContinue = false;  // flag to determinate if can continue with the porocess or not

        if (params != null && prop != null) {
            // test IgBLAST and HMMER binaires
            if (!prop.getProperty("blastBinPath").isEmpty() && !prop.getProperty("HmmerBinPath").isEmpty() && !prop.getProperty("UsearchBin").isEmpty()) {
                File blastFile = new File(prop.getProperty("blastBinPath"));
                File hmmerFile = new File(prop.getProperty("HmmerBinPath"));
                File usearchFile = new File(prop.getProperty("UsearchBin"));

                if (blastFile.exists() && hmmerFile.exists() && usearchFile.exists()) {
                    String sep = PlatformUtils.getSeparator();
                    File blastBin = new File(blastFile.toString() + sep + "igblastn");
                    File hmmerBin = new File(hmmerFile.toString() + sep + "hmmsearch");
                    File usearchBin = new File(usearchFile.toString() + sep + "usearch");

                    if (blastBin.exists() && blastBin.canExecute()) {
                        blast = true;
                        System.out.println("IgBLAST:\t" + blast);
                    } else {
                        System.err.println("IgBLAST:\t" + blast + "\t" + blastBin.toString());
                        System.exit(1);
                    }
                    if (hmmerBin.exists() && hmmerBin.canExecute()) {
                        hmmer = true;
                        System.out.println("hmmsearch:\t" + hmmer);
                    } else {
                        System.err.println("hmmsearch:\t" + hmmer + "\t" + hmmerBin.toString());
                        System.exit(1);
                    }
                    if (usearchBin.exists() && usearchBin.canExecute()) {
                        usearch = true;
                        System.out.println("Usearch:\t" + usearch);
                    } else {
                        System.err.println("Usearch:\t" + usearch + "\t" + usearchBin.toString());
                        System.exit(1);
                    }
                }
            }

            /* DATABASES */
            // human heavy chain
            if (!prop.getProperty("HumanHeavyChainVDataBase").isEmpty()
                    && !prop.getProperty("HumanHeavyChainDDataBase").isEmpty()
                    && !prop.getProperty("HumanHeavyChainJDataBase").isEmpty()) {

                File vdb = new File(prop.getProperty("HumanHeavyChainVDataBase"));
                File ddb = new File(prop.getProperty("HumanHeavyChainDDataBase"));
                File jdb = new File(prop.getProperty("HumanHeavyChainJDataBase"));

                if (vdb.exists() && vdb.isFile() && ddb.exists() && ddb.isFile() && jdb.exists() && jdb.isFile()) {
                    humanHCDataBases = true;
                } else {
                }
            }

            // human kappa chains
            if (!prop.getProperty("HumanKappaVDataBase").isEmpty()
                    && !prop.getProperty("HumanKappaJDataBase").isEmpty()
                    && !prop.getProperty("HumanHeavyChainDDataBase").isEmpty()) {

                File vdb = new File(prop.getProperty("HumanKappaVDataBase"));
                File ddb = new File(prop.getProperty("HumanHeavyChainDDataBase"));
                File jdb = new File(prop.getProperty("HumanKappaJDataBase"));

                if (vdb.exists() && vdb.isFile() && ddb.exists() && ddb.isFile() && jdb.exists() && jdb.isFile()) {
                    humanKCDataBases = true;
                }
            }

            // human lamda chains
            if (!prop.getProperty("HumanLamdaVDataBase").isEmpty()
                    && !prop.getProperty("HumanLamdaJDataBase").isEmpty()
                    && !prop.getProperty("HumanHeavyChainDDataBase").isEmpty()) {

                File vdb = new File(prop.getProperty("HumanLamdaVDataBase"));
                File ddb = new File(prop.getProperty("HumanHeavyChainDDataBase"));
                File jdb = new File(prop.getProperty("HumanLamdaJDataBase"));

                if (vdb.exists() && vdb.isFile() && ddb.exists() && ddb.isFile() && jdb.exists() && jdb.isFile()) {
                    humanLCDataBases = true;
                }
            }

            // mouse databases
            if (!prop.getProperty("MouseHeavyChainVDataBase").isEmpty()
                    && !prop.getProperty("MouseHeavyChainDDataBase").isEmpty()
                    && !prop.getProperty("MouseHeavyChainJDataBase").isEmpty()) {

                File vdb = new File(prop.getProperty("MouseHeavyChainVDataBase"));
                File ddb = new File(prop.getProperty("MouseHeavyChainDDataBase"));
                File jdb = new File(prop.getProperty("MouseHeavyChainJDataBase"));

                if (vdb.exists() && vdb.isFile() && ddb.exists() && ddb.isFile() && jdb.exists() && jdb.isFile()) {
                    mouseHCDataBases = true;
                }
            }

            if (!prop.getProperty("MouseKappaVDataBase").isEmpty() && !prop.getProperty("MouseHeavyChainDDataBase").isEmpty() && !prop.getProperty("MouseKappaJDataBase").isEmpty()) {
                File vdb = new File(prop.getProperty("MouseKappaVDataBase"));
                File ddb = new File(prop.getProperty("MouseHeavyChainDDataBase"));
                File jdb = new File(prop.getProperty("MouseKappaJDataBase"));

                if (vdb.exists() && vdb.isFile() && ddb.exists() && ddb.isFile() && jdb.exists() && jdb.isFile()) {
                    mouseKCDataBases = true;
                }
            }

            if (!prop.getProperty("MouseLamdaVDataBase").isEmpty() && !prop.getProperty("MouseHeavyChainDDataBase").isEmpty() && !prop.getProperty("MouseLamdaJDataBase").isEmpty()) {
                File vdb = new File(prop.getProperty("MouseLamdaVDataBase"));
                File ddb = new File(prop.getProperty("MouseHeavyChainDDataBase"));
                File jdb = new File(prop.getProperty("MouseLamdaJDataBase"));

                if (vdb.exists() && vdb.isFile() && ddb.exists() && ddb.isFile() && jdb.exists() && jdb.isFile()) {
                    mouseLCDataBases = true;
                }
            }

            /* HMMER profils */
            //human
            if (!prop.getProperty("HumanHmmerHCVProfile").isEmpty() && !prop.getProperty("HumanHmmerHCJProfile").isEmpty()) {
                File vProfil = new File(prop.getProperty("HumanHmmerHCVProfile"));
                File jProfil = new File(prop.getProperty("HumanHmmerHCJProfile"));

                if (vProfil.exists() && jProfil.exists()) {
                    humanHCProfils = true;
                }
            }

            if (!prop.getProperty("HumanHmmerKVProfil").isEmpty() && !prop.getProperty("HumanHmmerKJProfil").isEmpty()) {
                File vProfil = new File(prop.getProperty("HumanHmmerKVProfil"));
                File jProfil = new File(prop.getProperty("HumanHmmerKJProfil"));

                if (vProfil.exists() && jProfil.exists()) {
                    humanKCProfils = true;
                }
            }

            if (!prop.getProperty("HumanHmmerLVProfil").isEmpty() && !prop.getProperty("HumanHmmerLJProfil").isEmpty()) {
                File vProfil = new File(prop.getProperty("HumanHmmerLVProfil"));
                File jProfil = new File(prop.getProperty("HumanHmmerLJProfil"));

                if (vProfil.exists() && jProfil.exists()) {
                    humanLCProfils = true;
                }
            }

            //mouse
            if (!prop.getProperty("MouseHmmerHCVProfile").isEmpty() && !prop.getProperty("MouseHmmerHCJProfile").isEmpty()) {
                File vProfil = new File(prop.getProperty("MouseHmmerHCVProfile"));
                File jProfil = new File(prop.getProperty("MouseHmmerHCJProfile"));

                if (vProfil.exists() && jProfil.exists()) {
                    mouseHCProfils = true;
                }
            }

            if (!prop.getProperty("MouseHmmerKVProfil").isEmpty() && !prop.getProperty("MouseHmmerKJProfil").isEmpty()) {
                File vProfil = new File(prop.getProperty("MouseHmmerKVProfil"));
                File jProfil = new File(prop.getProperty("MouseHmmerKJProfil"));

                if (vProfil.exists() && jProfil.exists()) {
                    mouseKCProfils = true;
                }
            }

            if (!prop.getProperty("MouseHmmerLVProfil").isEmpty() && !prop.getProperty("MouseHmmerLJProfil").isEmpty()) {
                File vProfil = new File(prop.getProperty("MouseHmmerLVProfil"));
                File jProfil = new File(prop.getProperty("MouseHmmerLJProfil"));

                if (vProfil.exists() && jProfil.exists()) {
                    mouseLCProfils = true;
                }
            }

            // verify if there are some databases configured
            System.out.println("\nSpecies:\t" + params.getString("species"));
            System.out.println("Type of chain:\t" + params.getString("type"));
            if (params.getString("species").equalsIgnoreCase("human")) {
                // verify databases and profils for human

                if (params.getString("type").equalsIgnoreCase("hc")) {
                    if (humanHCDataBases && humanHCProfils) {
                        System.out.println("Data Bases:\t" + humanHCDataBases);
                        System.out.println("Profils:\t" + humanHCProfils);

                        canContinue = true;
                    } else {
                        System.out.println("Data Bases:\t" + humanHCDataBases);
                        System.out.println("Profils:\t" + humanHCProfils);
                    }
                } else if (params.getString("type").equalsIgnoreCase("kc")) {
                    if (humanKCDataBases && humanKCProfils) {
                        System.out.println("Data Bases:\t" + humanKCDataBases);
                        System.out.println("Profils:\t" + humanKCProfils);

                        canContinue = true;
                    } else {
                        System.out.println("Data Bases:\t" + humanKCDataBases);
                        System.out.println("Profils:\t" + humanKCProfils);
                    }
                } else if (params.getString("type").equalsIgnoreCase("lc")) {
                    if (humanLCDataBases && humanLCProfils) {
                        System.out.println("Data Bases:\t" + humanLCDataBases);
                        System.out.println("Profils:\t" + humanLCProfils);

                        canContinue = true;
                    } else {
                        System.out.println("Data Bases:\t" + humanLCDataBases);
                        System.out.println("Profils:\t" + humanLCProfils);
                    }
                }

            } else {
                // verify databases and profils for mouse
                if (params.getString("type").equalsIgnoreCase("hc")) {
                    if (mouseHCDataBases && mouseHCProfils) {
                        System.out.println("Data Bases:\t" + mouseHCDataBases);
                        System.out.println("Profils:\t" + mouseHCProfils);

                        canContinue = true;
                    } else {
                        System.out.println("Data Bases:\t" + mouseHCDataBases);
                        System.out.println("Profils:\t" + mouseHCProfils);
                    }
                } else if (params.getString("type").equalsIgnoreCase("kc")) {
                    if (mouseKCDataBases && mouseKCProfils) {
                        System.out.println("Data Bases:\t" + mouseKCDataBases);
                        System.out.println("Profils:\t" + mouseKCProfils);

                        canContinue = true;
                    } else {
                        System.out.println("Data Bases:\t" + mouseKCDataBases);
                        System.out.println("Profils:\t" + mouseKCProfils);
                    }
                } else if (params.getString("type").equalsIgnoreCase("lc")) {
                    if (mouseLCDataBases && mouseLCProfils) {
                        System.out.println("Data Bases:\t" + mouseLCDataBases);
                        System.out.println("Profils:\t" + mouseLCProfils);

                        canContinue = true;
                    } else {
                        System.out.println("Data Bases:\t" + mouseLCDataBases);
                        System.out.println("Profils:\t" + mouseLCProfils);
                    }
                }

            }
        }

        return canContinue;
    }

    /**
     * @param inputFile input Fastq file
     * @param ouputPath File to output path
     * @return File to library directory
     */
    private static File makeDirectoryStructure(File inputFile, File outputPath)
            throws IOException {
        File libDir = null;

        if (inputFile.exists() && inputFile.canRead()) {
            if (outputPath.exists() && outputPath.canWrite()) {
                if (inputFile.length() > 0) {
                    String libName = FilenameUtils.getBaseName(inputFile.getName());

                libDir = new File(outputPath.getAbsolutePath() + PlatformUtils.getSeparator() + libName);
                if (libDir.mkdirs()) {
                    FileUtils.copyFileToDirectory(inputFile, libDir);
                }
                }
                
            }
        }

        return libDir;
    }

    public static void main(String[] args) {
        // parse commandline paramas
        Namespace params = parseParams(args);
        if (params != null) {
            if (params.getBoolean("window")) {
                System.out.println("=========================================");
                System.out.println("\tImmunediversity v" + version);
                System.out.println("\tUser interface");
                System.out.println("=========================================\n");
            } else {
                System.out.println("=========================================");
                System.out.println("\tImmunediversity v" + version);
                System.out.println("=========================================\n");
                // load properties file
                File configFile = new File(params.getString("conf"));

                if (configFile.exists() && configFile.canRead()) {
                    Properties prop = readProperties(configFile);

                    if (testConfiguration(params, prop)) {
                        File inputFile = new File(params.getString("input"));
                        File outputPath = new File(params.getString("output"));

                        File libDir = null;
                        try {
                            libDir = makeDirectoryStructure(inputFile, outputPath);
                        } catch (IOException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        if (libDir != null) {
                            System.out.println("\n=========================================");
                            System.out.println("Processing:\t" + libDir.getName());
                            Date start = new Date();
                            System.out.println("Start:\t" + start.toString());

                            if (params.getString("type").equalsIgnoreCase("hc")) {
                                HeavyChainWorkFlow hcw = new HeavyChainWorkFlow();
                                String sep = PlatformUtils.getSeparator();

                                hcw.setParams(params);
                                hcw.setProp(prop);

                                System.out.println("\n >IgBLAST process");
                                File blastFile = new File(libDir.toString() + sep + libDir.getName() + ".fastq");
                                LinkedHashMap<String, HeavyChain> heavyChains = hcw.runIgBlast(blastFile, "sanger");
                                
                                System.out.println("\n >Cut only VDJ segments");
                                File fastqVDJfile = new File(libDir.toString() + sep + libDir.getName());
                                try {
                                    fastqVDJfile = hcw.getOnlyVDJ(heavyChains, fastqVDJfile.toString(), blastFile, "sanger");
                                } catch (IOException ex) {
                                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                System.out.println("\n >Filtering process");
                                File filteredFastqFile = hcw.filters(fastqVDJfile, 200, 28);  // object, min length, min quality

                                System.out.println("\n >HMMER process");
                                Reference2ObjectOpenHashMap<String, HMMERout> hmmerResults = null;
                                try {
                                    hmmerResults = hcw.runHmmer(filteredFastqFile, filteredFastqFile.getParentFile(), "sanger");
                                } catch (Exception ex) {
                                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                System.out.println("\n >get CDR3 and filter sequences without CDR3 sequence");
                                LinkedHashMap<String, HeavyChain> heavyChainsWCDR3 = null;
                                if (hmmerResults != null && heavyChains != null) {
                                    Readers read = new Readers();
                                    LinkedHashMap<String, DNASequence> filteredSequences = null;
                                    try {
                                        filteredSequences = read.readFastq2LinkedHashMap(filteredFastqFile, "sanger");
                                    } catch (IOException ex) {
                                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    //heavyChainsWCDR3 = hcw.setCDR3(hmmerResults, heavyChains, filteredSequences);
                                    heavyChainsWCDR3 = hcw.removeSequencesWithoutCDR3(heavyChainsWCDR3);
                                    System.out.println(heavyChainsWCDR3.size() + " sequences with CDR3");

                                    Writers write = new Writers();
                                    try {
                                        write.writeVDJTSVFile(heavyChains, new File(libDir.toString() + sep + "VDJs.txt"));
                                        write.writeCDR3file(heavyChains, new File(libDir.toString() + sep + "CDR3s.fa"));
                                    } catch (IOException ex) {
                                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                } else {
                                    System.err.println("ERROR: no more data to process!!!");
                                }

                                System.out.println("\n >group by rearrangement VJ");
                                if (heavyChainsWCDR3 != null) {
                                    LinkedHashMap<String, Rearrangement> rearrangements = hcw.groupByRearrangement(heavyChainsWCDR3);

                                    if (rearrangements != null) {
                                        System.out.println(rearrangements.size() + " rearrangements");

                                        System.out.println("Clonal groups");
                                        System.out.println("  >Usearch process");
                                        LinkedHashMap<String, ClonalGroupTemp> cg = new LinkedHashMap();
                                        try {
                                            cg = hcw.doClonalGroups(rearrangements);
                                        } catch (Exception ex) {
                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        if (!cg.isEmpty()) {
                                            Writers write = new Writers();
                                            try {
                                                write.writeClonalGropsFile(cg, libDir.getName(), new File(libDir.toString() + sep + "groupsVJs.txt"));
                                                write.writeClonalGroupsConsensusFile(cg, libDir.getName(), new File(libDir.toString() + sep + "consensusVJ.fa"));
                                            } catch (IOException ex) {
                                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            
                                            System.out.println("\nLineages");
                                            System.out.println("  >Usearch process 2");
                                            LinkedHashMap<String, Lineage> lineages = new LinkedHashMap();
                                            try {
                                                lineages = hcw.doLineages(cg);
                                            } catch (IOException ex) {
                                                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                            
                                            if (!lineages.isEmpty()) {
                                                File consensusFullVJ = new File(libDir.toString() + sep + "consensusFullVJ.txt");
                                                try {
                                                    write.writeGroupsFullVJ(lineages, libDir.getName(), new File(libDir.toString() + sep + "groupsFullVJ.txt"));
                                                    write.writeConsensusFullVJ(lineages, libDir.getName(), consensusFullVJ);
                                                } catch (IOException ex) {
                                                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                                
                                                // assigning the right Allele to consensus
                                                if (consensusFullVJ.exists() && consensusFullVJ.canRead()) {
                                                    System.out.println("\n >IgBLAST process 2");
                                                    LinkedHashMap<String, HeavyChain> consensusHeavyChains = hcw.correctAlleleAssignation(consensusFullVJ);
                                                    
                                                    if (!consensusHeavyChains.isEmpty()) {
                                                        LinkedHashMap<String, DNASequence> consensusReads = new LinkedHashMap<>();
                                                        Readers read = new Readers();
                                                        
                                                        try {
                                                            consensusReads = read.readDNAFasta(consensusFullVJ);
                                                            write.writeVDJTSVFile(consensusHeavyChains, new File(libDir.toString() + sep + "Alleles_consensusfullVDJ.txt"));
                                                        } catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                        } 
                                                        
                                                        consensusHeavyChains = hcw.setSequences(consensusHeavyChains, consensusReads);
                                                        consensusHeavyChains = hcw.removeConsensusOutOfFrame(consensusHeavyChains);
                                                        
                                                        Set tempset = consensusHeavyChains.keySet();
                                                        Iterator<String> tempit = tempset.iterator();
                                                        
                                                        while (tempit.hasNext()) {
                                                            HeavyChain temphc = consensusHeavyChains.get(tempit.next());
                                                            //temphc.print();
                                                        }
                                                        System.out.println("elements: " + consensusHeavyChains.size());
                                                        
                                                        try {
                                                            //write.writeOnlyV(consensusHeavyChains, new File(libDir.toString() + sep + "onlyV.fa"));
                                                            LinkedList<String> mutations = hcw.getMutationsRate(consensusHeavyChains);
                                                            write.writeMutationsFile(mutations, new File(libDir.toString() + sep + "kaks.txt"));
                                                        } catch (IOException ex) {
                                                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    System.err.println("ERROR: no more data to work");
                                }

                            } else if (params.getString("type").equalsIgnoreCase("kc")) {

                            } else if (params.getString("type").equalsIgnoreCase("lc")) {

                            }

                            Date end = new Date();
                            System.out.println("\n\nEND:\t" + end.toString());

                            Map diff = getDifference(start, end);
                            System.out.println("\nFinished in:\t" + diff.get("Hrs") + "Hrs " 
                                    + diff.get("Min") + "Min " + diff.get("Sec") + "Secs");
                            System.out.println("=========================================\n");
                        }
                    } else {
                        System.err.println("\nERROR: Configuration file not correct!");
                        System.err.println("Process not completed!!!");
                    }
                }
            }
        }
    }

}
